#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os, io
import pyedflib
import csv
import numpy as np
from itertools import zip_longest
from .edfio import EDFIO

import pprint
pp = pprint.PrettyPrinter(indent=4)

# http://pyedflib.readthedocs.io/en/latest/
# https://github.com/holgern/pyedflib/blob/master/pyedflib/edfwriter.py


class EDF(EDFIO):

    def __init__(self, param={}):
        # применяем конструктор родителя
        super(EDF, self).__init__()

        self.result = None

        if not param or type(param) != dict:
            return

        fname_debug = param.get('debug')
        if fname_debug:
            res = self.open_file(fname_debug, 'a', 'debug')
            if res != 0:
                print('cant open debug file', str(res))

        return


    # деструктор
    def __del__(self):
        ftypes = []
        for ftype in self.files:
            ftypes.append(ftype)
        for ftype in ftypes:
            self.close_file(ftype)
        return


    # прочитать канал. Устаревшее, надо удалить и заменить в использующем коде на взятие данных канала
    def read_signal(self, channel):
        if type(channel) != int or channel < 0:
            return self.set_error(109, "Invalid parameter 'channel', must be int")

        edf = self.files.get('in', {}).get('edf')
        params = self.files.get('in', {}).get('params')
        if not edf or not params:
            return self.set_error(110, 'EDF / BDF file is not open, cant read signal')

        sig_count = params.get('signal_count', 0)
        if sig_count < 1:
            return self.set_error(111, 'There is no signals in EDF / BDF file')

        if channel >= sig_count:
            return self.set_error(112, "'channel'={c} is greater then signals count in file".format(c=channel))

        self.result = list(edf.readSignal(channel))
        return self.set_error()


    # добавить канал в один из открытых файлов
    def add_channel(
        self,
        typ,            # file type
        summary,        # channel summary
        data,           # channel data
    ):
        if not typ or type(typ) != str:
            return self.set_error(102, "Invalid file typ")
        if typ not in self.files:
            return self.set_error(110, "Typ is not set")

        p = self.files[typ].get('params')
        if not p:
            return self.set_error(111, "File for typ=%s not set" % typ)

        #print('out_add_channel data_len=%s' % len(data))
        #pp.pprint(summary)

        p['canals'].append(summary)
        p['canal_data'].append(data)
        if 'canal_names' not in p:
            p['canal_names'] = {}
        p['canal_names'][summary['label']] = len(p['canals']) - 1
        p['signal_count'] += 1
        return self.set_error()


    # добавить канал в выходной файл
    def out_add_channel(
        self,
        summary,        # channel summary
        data,           # channel data
    ):
        return self.add_channel(
            typ='out',
            summary=summary,
            data=data,
        )


    # удалить канал в одном из открытых файлов
    def delete_channel(
        self,
        typ,            # file type
        number=None,    # channel number
        name=None,      # channel name
    ):
        if not typ or type(typ) != str:
            return self.set_error(102, "Invalid file typ=%s" % typ)
        if name == None and number == None:
            return self.set_error(301, "Cnannel name and number is not set")

        params = self.files.get(typ, {}).get('params')
        if params == None:
            return self.set_error(302, "Have not data read")

        canal_datas = params.get('canal_data')
        canal_count = len(canal_datas)
        if canal_datas == None or canal_count == 0:
            return self.set_error(303, "Have not channels")

        canal_num = -1
        if number != None:
            # прямо задан номер канала
            canal_num = number

        else:
            res = self.get_channel_num_by_name(typ, name)
            if res != 0:
                return res
            canal_num = self.last_result

        if canal_num == -1:
            return self.set_error(304, "Cant find channel with name=%s" % name)

        if canal_num >= canal_count:
            return self.set_error(305, "Channel_num=%s is out of range!" % canal_count)

        # нашли канал, проверки успешны. Удаляем его
        del params['canal_data'][canal_num]
        del params['canals'][canal_num]

        # пересчитываем хеш с именями-номерами каналов
        params['canal_names'] = {}
        for i in range(len(params['canals'])):
            canal = params['canals'][i]
            label = canal['label']
            params['canal_names'][label] = i

        return self.set_error()


    # получить номер канала по имени
    def get_channel_num_by_name(
        self,
        typ,            # file type
        name,           # channel name
    ):
        if not typ or type(typ) != str:
            return self.set_error(102, "Invalid file typ")
        if name == None:
            return self.set_error(301, "File channel name is not set")

        params = self.files.get(typ, {}).get('params')
        if params == None:
            return self.set_error(302, "Have not data for file typ = %s" % typ)

        canal_names = params.get('canal_names')
        if not canal_names:
            return self.set_error(303, "File have not channels")

        canal_num = canal_names.get(name, -1)
        if canal_num == -1:
            return self.set_error(304, "Cant find channel with name=%s" % name)

        self.last_result = canal_num
        return self.set_error()


    # получить данные канала по имени или номеру
    def get_channel_data(
        self,
        typ='data',     # file type
        number=None,    # channel number
        name=None,      # channel name
    ):
        if not typ or type(typ) != str:
            return self.set_error(102, "Invalid file typ")
        if name == None and number == None:
            return self.set_error(301, "Cnannel name and number is not set")

        params = self.files.get(typ, {}).get('params')
        if params == None:
            return self.set_error(302, "Have not data read")

        canal_datas = params.get('canal_data')
        canal_count = len(canal_datas)
        if canal_datas == None or canal_count == 0:
            return self.set_error(303, "Have not channels")

        canal_num = -1
        if number != None:
            # прямо задан номер канала
            canal_num = number

        else:
            res = self.get_channel_num_by_name(typ, name)
            if res != 0:
                return res
            canal_num = self.last_result

        if canal_num == -1:
            return self.set_error(304, "Cant find channel with name=%s" % name)

        if canal_num >= canal_count:
            return self.set_error(305, "Channel_num=%s is out of range!" % canal_count)

        self.last_result = np.asarray(canal_datas[canal_num])
        return self.set_error()



class BDF(EDF):
    pass



"""
    AUTHOR - Мартынов Сергей
    writemehere@bk.ru
"""
