#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys, traceback
import glob
import pyedflib
import tempfile
import shutil
import csv
import io
import struct
from datetime import datetime
from copy import deepcopy
from itertools import zip_longest
import numpy as np

import pprint
pp = pprint.PrettyPrinter(indent=4)

try:
    import logging
    from settings import *
    logger = logging.getLogger(__name__)
    logger.addHandler(logging.FileHandler(LOG_FILE))
    logger.setLevel(logging.INFO)
except:
    logger = False

class EDFIO(object):

    def __getitem__(self, key):
        return getattr(self, key)
    def __setitem__(self, key, val):
        return setattr(self, key, val)

    # обновить информацию о последней ошибке
    def set_error(self, status=0, text=None):
        self.status = status
        self.error = text
        return status

    def Logg(self,  mess):
        if logger:
            logger.info(
                "[{pid}] {time} {mess}".format(
                    pid=os.getpid(),
                    time=datetime.strftime(datetime.now(), "%Y.%m.%d %H:%M:%S.%f"),
                    mess=mess,
                )
            )
        else:
            print(
                "[{pid}] {time} {mess}".format(
                    pid=os.getpid(),
                    time=datetime.strftime(datetime.now(), "%Y.%m.%d %H:%M:%S.%f"),
                    mess=mess,
                )
            )


    def __init__(self):
        self.edf = None
        self.pos = 0
        self.balert = False
        self.neurochat = False
        self.files = {}
        self.extension = None
        self.last_result = None
        self.set_error()

    # получить все файлы, соотвествующие маске
    def get_files_on_mask(self, mask):
        files = glob.glob(mask)
        return files

    # высрать чонить в дебуг
    def debug(self, text):

        debug_file = self.files.get('debug').get('file')
        if not debug_file:
            return self.set_error(103, "debug file not opened")

        dt = '{0:%Y-%m-%d %H:%M:%S} : '.format(datetime.now())

        if type(text) == str:
            text = dt + text + '\n'
            debug_file.write(text)
        else:
            self.pp.pprint(text)
            debug_file.write('\n')

        return self.set_error()

    # прочитать из буфера length байтов, продвинуть позицию
    def read_from_buf(self, buf, length):
        ans = buf[self.pos : self.pos + length]
        self.pos += length

        # костыли на обработку кривых данных balert
        if self.balert:
            if ans == b'-3276.8 ':
                ans = b'-32768 '
            if ans == b'3276.7  ':
                ans = b'32767  '

        return ans

    # загрузка данных полиграфа энергии. Она умеет писать только DAT, поэтому отдельный загрузчик
    def load_energy(self, filename):
        self.Logg('Load Energy data file=%s' % filename)

        rawfile = open(filename, "rb")
        (sens, channel_num) = struct.unpack("IB", rawfile.read(5))

        if (channel_num != 8 or sens != 10):
            return self.set_error(
                1,
                'Invalid file type (Energy). Channels != 8 or sens != 10'
            )

        # надо для совместимости сделать вид что мы загрузили эти данные бдфом
        params                          = {}
        params['freq']                  = 100
        params['file_type']             = 'BDF'
        params['local_subject_id']      = ''
        params['local_recording_id']    = ''
        params['date']                  = ''
        params['time']                  = ''
        params['header_len']            = 0
        params['version_data_format']   = 'BDF'
        params['bytes_per_sample']      = 3
        params['signal_count']          = 6
        params['bytes_per_record']      = 0
        params['canal_data']            = []
        params['canal_names']           = {}
        params['canals']                = []
        canals = [
            'SGR',
            'PPG1',
            'BR1',
            'BR2',
            'MOV',
            'PPG2',
        ]

        # read channels from DAT
        i = 0
        for ch in canals:
            (ch_size,) = struct.unpack("I", rawfile.read(4))
            params['canal_data'].append(
                np.frombuffer(
                    rawfile.read(ch_size * 2),
                    dtype=np.uint16,
                ).astype(float)
            )
            """
            self.Logg('Read canal #{i} label={l} size={s}'.format(
                i=i,
                l=ch,
                s=ch_size,
            ))
            """

            params['all_duration'] = float(ch_size / params['freq'])

            # заводим канал
            summary = {
                'label': ch,
                'sample_rate': params['freq'],
                'samples': params['freq'],
                'frequency': params['freq'],
                'physical_max': 8388607.0,
                'physical_min': -8388608.0,
                'digital_max': 8388607.0,
                'digital_min': -8388608.0,
                'transducer': '',
                'prefilter':''
            }
            if ch in ['SGR']:
                summary['dimension'] = 'usm'
            elif ch in ['PPG1', 'PPG2']:
                summary['dimension'] = 'pm'
            else:
                summary['dimension'] = 'XZ'

            params['bytes_per_record'] += summary['samples'] * params['bytes_per_sample']
            params['canal_names'][summary['label']] = i
            params['canals'].append(summary)
            i += 1

        # число рекордов = количеству секунд записи
        params['record_count'] = int(params['all_duration'])
        if params['record_count'] != params['all_duration']:
            # не целое количество секунд, докидываем 1 рекорд
            params['record_count'] += 1

        self.Logg('Load OK')
        rawfile.close()

        self.last_result = params
        return self.set_error()


    # прочитать из буфера заголовок файла БДФ \ ЕДФ
    def read_header(self, buf):
        if buf == None or len(buf) == 0:
            return self.set_error(101, "Empty header")

        params = {}
        self.pos = 0
        try:
            # разбираем заголовок
            f_type = None
            c = self.read_from_buf(buf, 1)
            s = self.read_from_buf(buf, 7).decode('ascii')

            if ord(c) == 255:
                # BDF branch
                f_type = 'BDF'
                if s != 'BIOSEMI':
                    return self.set_error(101, "Incorrect BDF header. BIOSEMI != '{s}'".format(s=s))

            elif ord(c) == 48:
                # EDF branch
                f_type = 'EDF'
                if s != '       ':
                    return self.set_error(101, "Incorrect EDF header. '       ' != '{s}'".format(s=s))

            else:
                return self.set_error(101, "Incorrect BDF / EDF header in file. First char={s}".format(s=ord(c)))

            params['file_type']          = f_type
            params['local_subject_id']   = self.read_from_buf(buf, 80).decode('ascii').strip()
            params['local_recording_id'] = self.read_from_buf(buf, 80).decode('ascii').strip()
            params['date']               = self.read_from_buf(buf, 8).decode('ascii')
            params['time']               = self.read_from_buf(buf, 8).decode('ascii')
            params['header_len']         = int(self.read_from_buf(buf, 8).decode('ascii').strip())
            params['version_data_format'] = self.read_from_buf(buf, 44).decode('ascii').strip()

            # количество рекордов
            params['record_count']       = int(self.read_from_buf(buf, 8).decode('ascii').strip())

            # длительность каждого рекорда, в секундах
            params['duration']           = float(self.read_from_buf(buf, 8).decode('ascii').strip())
            params['all_duration']       = params['record_count'] * params['duration']

            # количество каналов
            params['signal_count']       = int(self.read_from_buf(buf, 4).decode('ascii').strip())

            # сколько в 1 фрейме всего будет сэмплов
            params['samples']           = 0

            # сколько  байтов на сэмпл
            params['bytes_per_sample']  = 0
            if params['version_data_format'] in ['24BIT', 'BDF+C', 'BDF']:
                params['bytes_per_sample'] = 3
            elif params['version_data_format'] in ['BIOSEMI', 'EDF+C', 'EDF']:
                params['bytes_per_sample'] = 2
            else:
                if self.extension == 'edf':
                    params['bytes_per_sample'] = 2
                elif self.extension == 'bdf':
                    params['bytes_per_sample'] = 3

            params['canal_data'] = []
            params['canals'] = []

            # читаем заголовки каналов
            balert_ch = 0
            for i in range(params['signal_count']):
                canal = {}
                label = self.read_from_buf(buf, 16).decode('ascii').strip()
                canal['label'] = label
                params['canals'].append(canal)

                # каналы наличие которых может говорить о том, что это BAlert
                if label in ['ESUTimestamp', 'SystemTimestamp']:
                    balert_ch += 1

            # если ЕДФ - это может оказаться кривой BAlert
            if params['bytes_per_sample'] == 2:
                if params['local_recording_id'] == 'Neurotrend EEG record' or balert_ch == 2:
                    #self.Logg('It is BAlert!')
                    self.balert = True

            # распознаем нейрочат
            if params['bytes_per_sample'] == 2:
                if params['local_recording_id'] == 'Neurotrend EEG8 record':
                    #self.Logg('It is Neurochat!')
                    self.neurochat = True

            # читаем трансдюсеры каналов
            for i in range(params['signal_count']):
                canal = params['canals'][i]
                canal['transducer'] = self.read_from_buf(buf, 80).decode('ascii').strip()

            # читаем единицы измереная каналов
            for i in range(params['signal_count']):
                canal = params['canals'][i]
                canal['dimension'] = self.read_from_buf(buf, 8).decode('ascii').strip()

            # читаем физ минимумы каналов
            for i in range(params['signal_count']):
                canal = params['canals'][i]
                canal['physical_min'] = float(self.read_from_buf(buf, 8).decode('ascii').strip())

            # читаем физ максимумы каналов
            for i in range(params['signal_count']):
                canal = params['canals'][i]
                canal['physical_max'] = float(self.read_from_buf(buf, 8).decode('ascii').strip())

            # читаем цифровые минимумы каналов
            for i in range(params['signal_count']):
                canal = params['canals'][i]
                canal['digital_min'] = float(self.read_from_buf(buf, 8).decode('ascii').strip())

            # читаем цифровые максимумы каналов
            for i in range(params['signal_count']):
                canal = params['canals'][i]
                canal['digital_max'] = float(self.read_from_buf(buf, 8).decode('ascii').strip())

            # читаем префильтры каналов
            for i in range(params['signal_count']):
                canal = params['canals'][i]
                canal['prefilter'] = self.read_from_buf(buf, 80).decode('ascii').strip()

            # читаем количества сэмплов каналов, характерные для каждой записи
            for i in range(params['signal_count']):
                canal = params['canals'][i]
                canal['samples'] = int(self.read_from_buf(buf, 8).decode('ascii').strip())
                canal['frequency'] = int(canal['samples'] / params['duration'])
                #canal['sample_rate'] = canal['samples']
                canal['sample_rate'] = canal['frequency']

                # для каждого канала определяем gain
                phys = canal['physical_max'] - canal['physical_min']
                dig = canal['digital_max'] - canal['digital_min']
                if dig == 0.0:
                    dig = 1.0
                canal['gain'] = phys / dig

                # В балерте, данные каналы имеет неправильное кол-во сэмплов
                if self.balert:
                    if canal['label'] in ['ESUTimestamp', 'SystemTimestamp']:
                        # В кривом формате написано что в ESUTimestamp частота 512, в SystemTimestamp - 1024
                        # Но на самом деле частота в обоих 256 (как в 0 канале)
                        canal['samples'] = params['canals'][0]['samples']

                params['samples'] += canal['samples']

            # считаем сколько весят данные со всех каналов для одного record
            params['bytes_per_record'] = 0
            params['canal_names'] = {}
            for i in range(params['signal_count']):
                canal = params['canals'][i]
                samples = canal['samples']
                bytes = params['bytes_per_sample']

                # если формат файла балерт, то на разные каналы, разные данные, иначе стандарт
                if self.balert:
                    # ESUTimestamp - 4 bytes   SystemTimestamp - 8  all_other - 2 bytes
                    bytes = 2
                    if canal['label'] == 'ESUTimestamp':
                        bytes = 4
                    elif canal['label'] == 'SystemTimestamp':
                        bytes = 8

                """
                self.Logg('canal #{ci} name={n} samples={s} bytes={b}'.format(
                    ci=i,
                    n=canal['label'],
                    s=samples,
                    b=bytes
                ))
                """

                params['bytes_per_record'] += samples * bytes

                # каналы по именам
                params['canal_names'][canal['label']] = i

        except:

            exc_str = traceback.format_exc()
            return self.set_error(101, "Incorrect BDF / EDF header in file. Exception={e}".format(
                e=exc_str,
            ))

        self.last_result = params
        return self.set_error()


    # заглушка для обратной совместимости
    def read_data_new(self, buf, ):
        return self.read_data(buf)

    # прочитать данные из буфера (без хидера!). Ранее уже должен быть прочитан хидер!
    def read_data(
        self,
        buf,
        need_canals=[],
        device_name='',
    ):

        # проверяем что буфер не пустой
        if buf == None or len(buf) == 0:
            return self.set_error(101, "Empty buffer")

        # прочитать из файла данные ЕДФ / БДФ в соответствии с имеющимся заголовком
        header = self.files.get('header', {}).get('params')
        if not header:
            return self.set_error(110, "Cant read data-only file without header")

        if len(device_name) == 0:
            device_name = self.files['header']['device_name']

        # параметры для данного файла берутся из прочитанного хедера
        params = deepcopy(header)

        # сколько байт в одном сэмпле
        bit_format = header.get('version_data_format')
        bytes = header.get('bytes_per_sample')
        if bytes == 0:
            return self.set_error(101, "Incorrect BDF / EDF header, unknown format")

        # сколько описано каналов
        sig_count = header.get('signal_count', 0)
        if sig_count <= 0:
            return self.set_error(101, "Incorrect BDF / EDF header, no signal_count")

        canals = header.get('canals', [])
        canal_names = header.get('canal_names', [])
        if len(canals) != sig_count or len(canal_names) != sig_count:
            return self.set_error(101, "Incorrect BDF / EDF header, signals count != signal_count")

        duration = header.get('duration', 0)
        if duration <= 0:
            return self.set_error(101, "Incorrect BDF / EDF header, duration = 0")

        # подготавливаем пожелания по каналам которые надо прочесть - массив номеров
        need_canal_nums = []
        if type(need_canals) == list and len(need_canals) > 0:
            for nc in need_canals:
                if type(nc) in (int, float):
                    # индексы каналов оставляем как есть
                    c_num = int(nc)
                    if c_num < sig_count:
                        need_canal_nums.append(int(c_num))

                elif type(nc) == str:
                    # Если присланы имена - превращаем в индексы
                    c_num = header.get('canal_names', {}).get(nc)
                    if c_num and c_num < sig_count:
                        need_canal_nums.append(c_num)

        self.Logg('read_data need_canal_num=%s device_name=%s' % (need_canal_nums, device_name))

        canal_data = []
        record_size = header.get('bytes_per_record', 0)
        if record_size == 0:
            return self.set_error(101, "Incorrect BDF / EDF header, record size = 0")

        # инициализируем массив данных каналов
        for c_index in range(sig_count):
            canal_data.append([])

        # проверяем что в файле - целое число фреймов
        length = len(buf)
        iterations = length / record_size
        if iterations != int(iterations):
            return self.set_error(
                103,
                "Incorrect file format. file_size != n * record_size. Length={l} record_size={rs} iter={i}".format(
                    l=length,
                    rs=record_size,
                    i=iterations,
                )
            )

        # часть данных, до секунды, так может протеряться, тк мы потом в конце пишем с sample_rate = frequency и duration = 1s
        iterations = int(int(iterations * duration) / duration)

        self.pos = 0
        self.Logg('buf_size={fs} record_size={rs} iter={it}'.format(
            fs=length,
            rs=record_size,
            it=iterations,
        ))

        try:
            # пока все еще есть непрочитанные данные, читаем
            # !!!!!!!!!!!!!!!!!!! грустно но все в память !!!!!!!!!!!!!!!!!!!!!!
            read_count = 0
            for read_count in range(iterations):
                #self.Logg('Read #{i}'.format(i=read_count))

                # читаем данные каждого канала по указанному числу сэмплов
                for c_index in range(sig_count):
                    canal = canals[c_index]
                    samples = canal['samples']
                    gain = canal['gain']

                    # если формат файла балерт, то на разные каналы, разные данные, иначе стандарт
                    signed = False
                    canal_bytes = bytes
                    if self.balert:
                        signed = True
                        if canal['label'] == 'ESUTimestamp':
                            canal_bytes = 4
                        elif canal['label'] == 'SystemTimestamp':
                            canal_bytes = 8

                    if c_index == sig_count - 1:
                        # это канал аннотаций, текстовая инфа
                        txt = self.read_from_buf(buf, canal_bytes * samples)

                        # remember
                        #canal_data[c_index].append(txt)

                        continue

                    if (len(need_canal_nums) > 0 and c_index in need_canal_nums) or \
                       (len(need_canal_nums) == 0):

                        # канал из тех, которые надо прочитать. Читаем
                        for i in range(samples):
                            v = int.from_bytes(
                                self.read_from_buf(buf, canal_bytes),
                                byteorder='little',
                                signed=signed,
                            )

                            """
                            # decipher from bdf format - ДЛЯ НЕЙРОЧАТА
                            # функция распаковки 3 байт в double
                            auto unpack24BitValue = [](uchar *buf) -> double {
                                int intValue = ((buf[2] << 16) | (buf[1] << 8) | buf[0]);
                                if (intValue >= 8388608) intValue -= 16777216;
                                return (double)intValue;
                            };
                            # само конвертирование данные из фрэйма BDF в данные по каналам (j - канал, i - индекс значения)
                            frameData.Channels[j].Value[i] = (unpack24BitValue(ptr) * 0.000447) / 10.0 / 1000.0;
                            # далее это еще надо умножить на 1000000

                            if v >= 8388608:
                                v -= 16777216
                            v = int(v * 0.0447)
                            """

                            # Разная обработка данных для balert, edf, bdf
                            if bytes == 3:
                                if v >= 8388608:
                                    v -= 16777216
                                v = v * gain

                                # Только для нейрочатовской гарнитуры
                                if self.neurochat and c_index < 8:
                                    v = v / 1000.0

                            elif self.balert:
                                if c_index < 24:
                                    v = v / 10.0

                            else:
                                v = v * gain

                            # remember
                            canal_data[c_index].append(v)

                    else:

                        # канал читать не нужно, просто пропускаем данные
                        self.read_from_buf(buf, canal_bytes * samples)

            # to numpy
            for c_index in range(sig_count):
                canal_data[c_index] = np.array(canal_data[c_index])

            # все прочитали
            self.Logg('Read data - OK')
            #self.Logg('canal_data={c}'.format(c=canal_data))

            # применяем формулы приведения
            if device_name.lower() == 'ntrend wirstband':
                self.Logg('Convert Ntrend wirstband:')

                # work SGR
                canal_num = canal_names.get('SCR', -1)
                if canal_num == -1:
                    return self.set_error(101, "Incorrect BDF / EDF header, have not SCR channel")

                channel_SGR = canal_data[canal_num]
                #self.Logg('Old SGR:')
                #pp.pprint(canal_data[canal_num][0:20])

                #channel_SGR[np.abs(channel_SGR) < 0.1] = 0.1
                #channel_SGR = np.trunc(1000000 / (9.5 * channel_SGR))

                channel_SGR[channel_SGR < 0.1] = 0.1
                channel_SGR = 5966 + np.trunc(1000000 / (6.182 * channel_SGR ** 0.855))
                canal_data[canal_num] = channel_SGR

                #self.Logg('New SGR:')
                #pp.pprint(canal_data[canal_num][0:20])

                # work PPG
                canal_num = canal_names.get('PPG', -1)
                if canal_num == -1:
                    return self.set_error(101, "Incorrect BDF / EDF header, have not SCR channel")

                channel_PPG = canal_data[canal_num]
                #self.Logg('Old PPG:')
                #pp.pprint(canal_data[canal_num][0:20])

                channel_PPG[np.abs(channel_PPG) < 0.1] = 0.1
                channel_PPG = np.trunc(500 * channel_PPG + 32000)
                canal_data[canal_num] = channel_PPG

                #self.Logg('New PPG:')
                #pp.pprint(canal_data[canal_num][0:20])

            params['canal_data'] = canal_data

            # возможно надо для каждого канала в инфе проставить сколько мы там начитали
            pass

        except:

            exc_str = traceback.format_exc()
            return self.set_error(101, "Error reading BDF data from file! Exception={e}".format(
                e=exc_str,
            ))

        self.last_result = params
        return self.set_error()


    # открыть файло с определенными параметрами, запомнить его по названию типа
    def open_file(
        self,
        fname,          # файл который надо открыть
        right,          # права обращения с файлом. Только для debug и не зарезервированных типов открытия
        typ,            # тип открытия, зарезервировано: 'in', 'out', 'debug', 'header', 'data', 'energy'.
        offset=0,       # только для typ=data. смещение, в записях (records) файла bdf
        cnt=0,          # только для typ=data. количество записей (records) файла bdf, которые надо прочитать
        need_canals=[], # каналы которые надо прочитать. Массив их номерoв или имен, можно вперемешку
        device_name='', # название девайса. Если указано, могут применяться специфические формулы
    ):
        """
        'r'     open for reading (default)
        'w'     open for writing, truncating the file first
        'x'     open for exclusive creation, failing if the file already exists
        'a'     open for writing, appending to the end of the file if it exists
        'b'     binary mode
        't'     text mode (default)
        '+'     open a disk file for updating (reading and writing)
        """

        self.Logg('open_file {f} right={r} typ={t} offset={o} cnt={c} device_name="{d}"'.format(
            f=fname,
            r=right,
            t=typ,
            o=offset,
            c=cnt,
            d=device_name,
        ))

        if not fname or type(fname) != str:
            return self.set_error(102, "Invalid file name %s typ=%s" % (fname, type(fname)))
        if not right or type(right) != str:
            return self.set_error(102, "Invalid right")
        if not typ or type(typ) != str:
            return self.set_error(102, "Invalid file typ")

        typ = typ.lower()
        if self.files.get(typ) and typ != 'data':
            return self.set_error(101, typ + " file already opened")
        in_types = ['in', 'header', 'data', 'energy']
        if typ in in_types and not os.path.exists(fname):
            return self.set_error(102, "File not exists")

        f = edf = None
        params = {}
        self.extension = fname.split('.')[-1].lower()

        # ---------------------------- IN ----------------------------------
        if typ == 'in':
            # читаем edf / bdf и делаем сводку в словаре
            try:
                # Создаем временный файл для чтения копируем файл во временный,
                # путь временного файла даем pyedflib.EdfReader, при таком подходе
                # имена с кирилицей читает, иначе выводит ошибку для файлов с кирилицей
                temp = tempfile.NamedTemporaryFile()
                shutil.copyfile(fname, temp.name)
                temp.seek(0)
                edf = pyedflib.EdfReader(temp.name)
                params = {
                    'fname': fname,
                    'signal_count': int(edf.signals_in_file),
                    'duration': float(edf.file_duration),
                    'start_dt': edf.getStartdatetime(),
                    'record_count': edf.datarecords_in_file,
                    'annot_count': edf.annotations_in_file,
                    'canals': [],
                    'canal_data': [],
                    'canal_names': {},
                }
                for canal in range(edf.signals_in_file):
                    label = edf.getLabel(canal)
                    params['canals'].append({
                        'label': label,
                        'dimension': edf.getPhysicalDimension(canal),
                        'samples': int(edf.getNSamples()[canal]),
                        'sample_rate': int(edf.getSampleFrequency(canal)),
                        'physical_max': float(edf.getPhysicalMaximum(canal)),
                        'physical_min': float(edf.getPhysicalMinimum(canal)),
                        'digital_max': int(edf.getDigitalMaximum(canal)),
                        'digital_min': int(edf.getDigitalMinimum(canal)),
                        'prefilter': edf.getPrefilter(canal),
                        'transducer': edf.getTransducer(canal),
                    })
                    params['canal_names'][label] = len(params['canals'])

            except Exception as e:
                exc_str = traceback.format_exc()
                return self.set_error(100, "Error open for reading edf file {f}. {e}".format(
                    f=fname,
                    e=exc_str,
                ))

        # ---------------------------- OUT ----------------------------------
        elif typ == 'out':
            try:
                # check we can write file
                # файл мы откроем когда уже зафигачим все каналы, при его открытии надо знать количество каналов
                f = open(fname, 'w')
                f.close()

                params = {
                    'fname': fname,
                    'signal_count': 0,
                    'duration': 0,
                    'start_dt': None,
                    'record_count': 0,
                    'annot_count': 0,
                    'canals': [],
                    'canal_data': [],
                    'canal_names': {},
                }

            except Exception as e:
                exc_str = traceback.format_exc()
                return self.set_error(100, "Error open for wtiting edf file: " + exc_str)

        # ---------------------------- DEBUG ----------------------------------
        elif typ == 'debug':
            try:
                debug_folder = os.path.dirname(fname)
                if not os.path.exists(debug_folder):
                    os.makedirs(debug_folder)

                f = open(fname, right)
                self.pp = pprint.PrettyPrinter(
                    indent=4,
                    stream=f,
                )
            except:
                exc_str = traceback.format_exc()
                return self.set_error(100, "Cant open file {f}: {e}".format(
                    f=fname,
                    e=exc_str,
                ))

        # ---------------------------- HEADER ----------------------------------
        elif typ == 'header':
            # читаем файло с заголовком
            head_size = 100500          # столько точно должно хватить для заголовка бдф
            try:
                f = open(fname, 'rb')
                buf = f.read(head_size)
                f.close()
                f = None
            except:
                exc_str = traceback.format_exc()
                return self.set_error(100, "Cant open file {f}: {e}".format(
                    f=fname,
                    e=exc_str,
                ))

            ans = self.read_header(buf)
            if ans != 0:
                return ans

            params = self.last_result

        # ---------------------------- DATA ----------------------------------
        elif typ == 'data':
            # предполагаем что читаем из файла, содержащего заголовок
            header = self.files.get('header', {}).get('params', {})
            head_len = header.get('header_len', 0)
            if not header or head_len == 0:
                return self.set_error(110, "Cant read data-only file without header")

            record_size = header.get('bytes_per_record', 0)
            if record_size == 0:
                return self.set_error(101, "Incorrect BDF / EDF header, record size = 0")

            record_count = header.get('record_count', 0)
            if record_count == 0:
                return self.set_error(101, "Incorrect BDF / EDF header, record count = 0")

            if offset + cnt > record_count:
                return self.set_error(102, "Cant read offset + cnt more than record_count!")

            # смещение откуда читать. Учитывая выброшенный заголовок и заданное смещение
            seek_pos = head_len + offset * record_size

            # столько данных надо прочитать чтобы получить cnt записей
            if cnt > 0:
                size = record_size * cnt
            else:
                # не задано сколько прочесть записей. Читаем все что есть
                size = record_size * (record_count - offset)

            """
            self.Logg('record_size={rs} cnt={cnt} offset={o} head_len={hl}  seek_pos={sp} size={s}'.format(
                rs=record_size,
                cnt=cnt,
                o=offset,
                hl=head_len,
                sp=seek_pos,
                s=size,
            ))
            """

            # читаем файло
            try:
                f = open(fname, 'rb')           # !!!!!!!!!!!!!! чтение в память возможно огромного файла !!!!!!!!!!!!!!
                f.seek(seek_pos)                # пропускаем заголовок и отступ заданный
                buf = f.read(size)
                f.close()
                f = None
            except:
                exc_str = traceback.format_exc()
                return self.set_error(100, "Cant open file {f}: {e}".format(
                    f=fname,
                    e=exc_str,
                ))

            ans = self.read_data(buf, need_canals, device_name=device_name)
            if ans != 0:
                return ans

            params = self.last_result

        # ---------------------------- ENERGY ----------------------------------
        elif typ == 'energy':
            try:
                ans = self.load_energy(fname)
                if ans != 0:
                    return ans
            except:
                exc_str = traceback.format_exc()
                return self.set_error(100, "Cant open file {f}: {e}".format(
                    f=fname,
                    e=exc_str,
                ))

            params = self.last_result

        # ---------------------------- OTHER ----------------------------------
        else:
            try:
                f = open(fname, right)
            except:
                exc_str = traceback.format_exc()
                return self.set_error(100, "Cant open file {f}: {e}".format(
                    f=fname,
                    e=exc_str,
                ))

        self.files[typ] = {
            'file': f,
            'edf': edf,
            'params': params,
            'device_name': device_name,
        }
        return self.set_error()


    # сохранить выходной файл
    def out_save(self):
        # сохраняет данные в out-file
        typ = 'out'
        if typ not in self.files:
            return self.set_error(110, "Out file is not set, cant save")

        p = self.files[typ].get('params')
        if not p:
            return self.set_error(111, "Out file params are not set")

        signal_count = p.get('signal_count')
        if not signal_count or signal_count < 1:
            return self.set_error(112, "Incorrect signal count parameter")

        duration = p.get('duration', 0.1)
        if not duration:
            duration = 0.1

        canal_info = p.get('canals')
        if not canal_info or type(canal_info) != list or len(canal_info) != signal_count:
            return self.set_error(114, "Incorrect canals summary!")

        canal_data = p.get('canal_data')
        if not canal_data or type(canal_data) != list or len(canal_data) != signal_count:
            return self.set_error(115, "Incorrect canals data!")

        fname = p.get('fname')
        if not fname:
            return self.set_error(116, "Incorrect fname parameter!")

        try:
            # Создаем временный файл который не удаляется при закрытии
            # закрываем файл затем копируем временный файл
            # удаляем временный файл
            # без использования временного файла pyedflib.EdfWriter кирилица не работает
            fd, path = tempfile.mkstemp()
            f = pyedflib.EdfWriter(
                path,
                signal_count,
                file_type=pyedflib.FILETYPE_BDFPLUS,    # or edflib.FILETYPE_EDFPLUS
            )

            #f.setDatarecordDuration(duration * 100000)

            # всегда пишем длительность рекорда 1 секунда
            f.setDatarecordDuration(100000)             # 1 second

            # так как мы силком пишем длительность рекорда 1 сек, то sample_rate должна равняться частоте сигнала
            for ci in range(len(canal_info)):
                freq = canal_info[ci].get('frequency', 0)
                if freq:
                    canal_info[ci]['sample_rate'] = freq
                    canal_info[ci]['samples'] = freq

            # пишем инфу по каналам и сами каналы
            f.setSignalHeaders(canal_info)
            f.writeSamples(canal_data)

            f.close()
            shutil.copyfile(path, fname)
            os.remove(path)

        except Exception as e:
            exc_str = traceback.format_exc()
            #raise(e)
            return self.set_error(117, "Error writing edf file {f}: {e}".format(
                f=fname,
                e=exc_str,
            ))

        return self.set_error()


    # закрыть нах файл по его типу
    def close_file(
        self,
        typ,            # file type
    ):
        if not typ or type(typ) != str:
            return self.set_error(102, "Invalid file typ")
        if not self.files.get(typ):
            return self.set_error(103, typ + " file not opened")

        try:
            if self.files[typ]['file']:
                self.files[typ]['file'].close()
            if self.files[typ]['edf']:
                self.files[typ]['edf']._close()

        except:
            #return self.set_error(100, "Error closing file " + fname)
            pass

        del self.files[typ]
        return self.set_error(0)


    # Сохранить или вернуть EDF/BDF
    def save_bdf_to_csv(
        self,
        fin_path,
        fout_path=False,
        channel_count=24
    ):

        # открываем файл
        ans = self.open_file(fin_path, 'r', 'header')
        if ans != 0:
            return ans

        # загоняем файл в буфер и отрезаем заголовок - ПЕРЕДЕЛАТЬ У НАС ЭТО УЖЕ СТАНДАРТНО!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        with open(fin_path, 'rb') as f:
            buf = f.read()
            ans = self.read_data(
                buf[int(self.last_result['header_len']):]
            )
        if ans != 0:
            return ans

        # собираем полученные данные
        channel_name_list = []
        channel_data = []
        for i in range(int(self.last_result['signal_count'])):
            if i + 1 > channel_count:
                break
            channel_name_list.append(self.last_result['canals'][i]['label'])
            channel_data.append(self.last_result['canal_data'][i])
        export_data = zip_longest(*channel_data, fillvalue='')

        # создаем csv файл или помещаем в переменную
        try:
            if fout_path:
                with open(fout_path, 'w', newline='') as csvfile:
                    wr = csv.writer(csvfile, delimiter='\t')
                    wr.writerow(channel_name_list)
                    for data in export_data:
                        wr.writerow(data)
                return self.set_error()

            else:
                buffer_data = io.StringIO()
                wr = csv.writer(buffer_data, delimiter='\t')
                wr.writerow(channel_name_list)
                for data in export_data:
                    wr.writerow(data)
                self.csv = buffer_data.getvalue()
                return self.set_error()

        except:
            return self.set_error(
                103,
                "Error in create csv! Exception={e}".format(
                    e=sys.exc_info(),
                )
            )


    # режет последние прочитанные данные по отметкам времени
    def cut_bdf(
        self,
        typ,
        start_time,
        end_time,
        channel_list,
        out_format='csv',
    ):

        if not typ or typ not in self.files:
            return self.set_error(120, "Incorrect type")
        if len(channel_list) == 0:
            return self.set_error(120, "Channel list empty")

        data_list = []
        channel_name_list = []
        params = self.files[typ].get('params')
        if not params or type(params) != dict:
            return self.set_error(222, "Have not last_result")

        # для каждого переданного канала обрезаем данные
        for c in channel_list:
            if type(c) == str:
                res = self.get_channel_num_by_name(
                    typ='data',
                    name=c,
                )
                if res != 0:
                    return res
                c = self.last_result

            if c > len(params['canals']):
                return self.set_error(121, "Channel - {} not found".format(c))

            channel_name_list.append(params['canals'][c]['label'])

            # Находим кол-во записей в секунду для каждого канала
            duration = float(params['duration'])
            samples = params['canals'][c]['samples']
            record_sec = (1 / duration) * samples
            record_start = int(record_sec * start_time)
            record_end = int(record_sec * end_time)

            data = params['canal_data'][c][record_start:record_end]
            data_list.append(data)

        # Для разного формата - разная выдача данных
        if out_format == 'csv':
            export_data = zip_longest(*data_list, fillvalue='')
            buffer_data = io.StringIO()
            wr = csv.writer(buffer_data, delimiter='\t')
            wr.writerow(channel_name_list)
            for data in export_data:
                wr.writerow(data)
            self.csv = buffer_data.getvalue()
            return self.set_error()

        else:
            return self.set_error(122, "Unknow format = {}".format(
                out_format,
            ))



"""
    AUTHOR - Мартынов Сергей
    writemehere@bk.ru
"""
