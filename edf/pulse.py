#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
logger = logging.getLogger(__name__)
import pprint
pp = pprint.PrettyPrinter(indent=4)

import os
import sys
import json
from copy import deepcopy
import numpy as np

# почистить интервалы сердечных сокращений, подготовить статистику
def clean_pulse(
    num,                        # массив пиков
    frequency,                  # частота потока данных
    avg_size=10,                # размер массива для вычисления плавающего среднего
    debug=0,
):

    """
    Готовим массив интервалов, считаем среднее и стд отклонение
    """
    dts = np.array([])
    last = 0
    for p in np.nditer(num):
        dts = np.append(dts, p - last)
        last = p
    sto = round(np.std(dts), 2)
    avg = round(np.mean(dts), 2)
    print('std=%s avg=%s' % (sto, avg))

    """
    если стд отклонение слишком велико - пробуем фильтрануть и пересчитать пики
    """
    if sto > avg / 5:
        print('bad std!')
        # return -1

    sto = round(avg * 0.1, 2)

    """
    Каждый интервал, если он вылезает за пределы среднего более 3 стд отклонений
        - либо удаляем, если слишком мелкий
        - либо пытаемся разделить если большой
    """
    new_num = np.array([], dtype=int)       # новый массив пиков
    prev_arr = np.array([], dtype=int)      # предыдущие avg_size валидных пиков
    t_abs = 0                               # обновляемое абсолютное время
    v_abs = 0                               # обновляемый абс номер отсчета
    minute_stat = {}                        # поминутная статистика
    ans_data = []
    for i in range(0, len(num) - 1):
        val = num[i]
        v_abs = val
        if i >= 1:
            val = num[i] - num[i-1]
        dt = int(val / frequency * 1000)
        t_abs += dt

        if len(prev_arr) > avg_size:
            # оставляем последние avg_size знач
            prev_arr = prev_arr[-avg_size:]

            # у нас достаточно пред пиков, пересчитываем средн и стд отклонение
            avg = round(np.mean(prev_arr), 2)

            # в дальнейшем проверка на отклонение от среднего на 3 стд откл
            # превращаем эту проверку на отклонение на 30% от средн
            sto = round(avg * 0.1, 2)

        # проверяем чтобы интервалы не выходили за пределы 3 стд отклонений от среднего
        if val - avg < - 3 * sto or dt < 300:
            # короткий
            if debug > 0:
                print('t=%s val=%s (dt=%s) avg=%s std=%s short drop' % (t_abs, val, dt, avg, sto))
            continue
        if val - avg > 3 * sto or dt > 1500:
            # длинный
            if debug > 0:
                print('t=%s val=%s (dt=%s) avg=%s std=%s long drop' % (t_abs, val, dt, avg, sto))
            continue

        # не вышел за пределы 3 стд откл - добавляем интервал
        new_num = np.append(new_num, v_abs)
        if debug > 0:
            print('t=%s val=%s (dt=%s) avg=%s std=%s add' % (t_abs, val, dt, avg, sto))

        # добавляем валидный интервал к массиву последних валидных
        prev_arr = np.append(prev_arr, val)

        dat = {
            'num': v_abs,
            't': t_abs,
            'dt': dt,
        }
        ans_data.append(dat)

        minute = int(t_abs / (60 * 1000))
        if minute in minute_stat:
            # уже была статистика по этой минуте
            minute_stat[minute]['rr'].append(dt)

        else:
            # еще не было статистики по этой минуте
            minute_stat[minute] = {'rr': [dt,],}

        continue

    """
    Для каждой минуты вычисляем метрики:
        - СТД отклонение длительности вошедших в минуту кардиоинтервалов (STD) в секундах
        - Моду Мо - тот временной отрезок на которой пришлось самое большое число кардиоинтервалов, в секундах.
            Для этого берем временные отрезки, округленные до 50 мс, от самого короткого интервала до самого длинного,
            смотрим в какой отрезок попало больше всего интервалов.
        - Амплитуду моды (АМо) - число кардиоинтервалов, соответствующих значению моды, в % к объему выборки.
        - Индекс напряжения (Ин) по формуле  Ин = АМо / (2 * Мо * STD)
    """
    mod_div = 50
    for minute in minute_stat:
        st = minute_stat[minute]
        if len(st['rr']) == 0:
            continue

        st['min'] = min(st['rr'])
        st['max'] = max(st['rr'])
        st['std'] = round(np.std(st['rr']) / 1000, 3)

        # определяем отрезки для вычисления моды
        min_d = st['min'] // mod_div
        max_d = st['max'] // mod_div
        if st['max'] != mod_div * max_d:
            max_d += 1

        mods = {}
        for mod in range(min_d, max_d + 1):
            mods[mod] = 0

        # считаем сколько интервалов в какую моду попали
        for rr in st['rr']:
            mods[rr // mod_div] += 1

        # ищем максимальную
        max_m = 0
        max_i = 0
        for mod in mods:
            if max_m < mods[mod]:
                max_i = mod
                max_m = mods[mod]

        # тот временной отрезок на которой пришлось самое большое число кардиоинтервалов
        st['Mo'] = round((mod_div * (2 * max_i + 1) / 2) / 1000, 2)

        # число кардиоинтервалов, соответствующих значению моды, в % к объему выборки
        st['AMo'] = round(max_m / len(st['rr']) * 100, 2)

        # Индекс напряжения (Ин) по формуле  Ин = АМо / (2 * Мо * STD)
        st['In'] = 0
        if st['Mo'] * st['std'] != 0:
            st['In'] = round(st['AMo'] / (2 * st['Mo'] * st['std']), 2)

    """
    Oбогащаем данные посчитанными поминутными метриками
    """
    for i in range(len(ans_data)):
        dat = ans_data[i]

        # находим минуту с которой брать статистику
        t = dat['t']
        minute = int(t / (60 * 1000))
        st = minute_stat[minute]

        #
        for name in ('std', 'Mo', 'AMo', 'In'):
            dat[name] = st[name]

        ans_data[i] = dat

    #
    return 0, ans_data, new_num




"""
    AUTHOR - Мартынов Сергей
    writemehere@bk.ru
"""



