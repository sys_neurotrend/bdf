# coding=utf-8

import logging
logger = logging.getLogger(__name__)
import pprint
pp = pprint.PrettyPrinter(indent=4)

import os
import json
import random
import math
import struct
import datetime
import sys, traceback
import numpy as np
import pandas as pd
from scipy.interpolate import (BSpline, UnivariateSpline)
import scipy.signal as signal
from scipy.signal import (
    butter,
    filtfilt,
    lfilter,
    find_peaks,
)
from scipy.integrate import simps


#изменение частоты данных
def resampling(a, current_freq, new_freq):
    current_size = len(a)
    if not current_size or not current_freq:
        raise Exception('Invalid data in file while resampling frequency - zero array length or zero current frequency')
    coeff = new_freq / current_freq
    new_size = int(current_size * coeff)
    new_arr = np.zeros(new_size)

    for i in range(0, new_size):
        old_index = int(i / coeff)
        new_arr[i] = a[old_index]

    return new_arr

#кубический сплайн с замазкой
def cubspl(x, x2, y, p):
    """
    при вызове сплайна может быть ситуация, когда на входе нету достаточного количества данных
    если происходит такая херня, то сплайн у нас просто равен прямой

    в нашем случае это происходит при вызове envpeak_ppg - для построения верхней и нижней огибающей PPG
    поэтому отлавливаем эту ситуацию сразу для верхней и нижней огибающей, именно в envpeak_ppg

    https://stackoverflow.com/questions/32230362/python-interpolate-univariatespline-package-error-mk-failed-for-hidden-m
    You are trying to instantiate a UnivariateSpline object with a zero-length array
    The error is likely triggered by this line, which sets a hidden variable m to the length of x and checks that you have at least k+1 points where k is the spline degree (default is cubic, k=3)

    https://docs.scipy.org/doc/scipy/reference/generated/scipy.interpolate.UnivariateSpline.html
    """

    tmp = UnivariateSpline(x2, y)
    tmp.set_smoothing_factor(p)
    return tmp(x)

# удаление нулей, меняем на значения шедшие до них (автореконнект биобраслета)
# также возвращаем индексы земененных нулей
def clean_zero(dat):
    d = np.copy(dat)
    length = len(d)
    nulls = []

    # последний не 0 попавшийся в данных
    ld = 0

    for i in range(0, length):

        if d[i] != 0:
            # получили не ноль, запоминаем посл ненулевой
            ld = d[i]
            continue

        # получили 0, запоминаем его позицию
        nulls.append(i)

        # change data
        d[i] = ld

    # возвращаем данные без нулей и серии
    return d, nulls

#скользящая средняя
def movmean(arr, windows):
    ma = np.copy(arr)
    odd_fix = 0
    if (windows % 2 == 0):
        odd_fix = 1

    for i in range(0, ma.size):
        start = i - int(windows / 2)
        if (start < 0):
            start = 0

        finish = i + int(windows / 2) - odd_fix + 1
        if (finish > ma.size - 1):
            finish = ma.size

        ma[i] = arr[start:finish].mean()

    return ma

#удаление выбросов по гистограме
def clean_hist(arr, arr_size, left, right, zero):
    #print('Clean_hist arr=%s, arr_size=%s, left=%s, right=%s, zero=%s' % (arr, arr_size, left, right, zero))

    hh = 100
    [a, b] = np.histogram(arr, hh)

    """
    try:
        # arr=[nan nan nan ... nan nan  0.], arr_size=89981, left=0.01, right=0, zero=0 - exception
        [a, b] = np.histogram(arr, hh)
    except:
        # не удалось построить гистограмму, возвращаем что есть
        exc_str = traceback.format_exc()
        print('Exception in np.histogram(arr, hh), return as-is. Traceback: %s' % exc_str)
        return arr
    """

    a = a / arr_size

    if (right > 0):
        sum = 0
        for i in range(1, hh):
            sum = sum + a[hh-i]
            if (sum > right):
                for j in range(1, arr_size):
                    if (arr[j] > b[hh-i]):
                        arr[j] = zero
                break

    if (left > 0):
        sum = 0
        for i in range(1, hh):
            sum = sum + a[i]
            if (sum > left):
                for j in range(1, arr_size):
                    if (arr[j] < b[i]):
                        arr[j] = zero
                break

    return arr

def local_minmax(data):
    return np.diff(np.sign(np.diff(data))).nonzero()[0] + 1

def local_min(data):
    return (np.diff(np.sign(np.diff(data))) > 0).nonzero()[0] + 1

def local_max(data):
    return (np.diff(np.sign(np.diff(data))) < 0).nonzero()[0] + 1

# --------------------------------- FILTERS ------------------------------------
# фильтр с заданной полосой пропускания
def butter_band_pass(
    lowcut,
    highcut,
    freq,
    order=4
):
    #print('butter_band_pass', lowcut, highcut, freq, order)
    nyq = 0.5 * freq
    low = lowcut / nyq
    high = highcut / nyq
    #print('butter', order, low, high)
    b, a = butter(order, [low, high], btype='bandpass')
    return b, a

# фильтр с заданной полосой непропуска
def butter_band_stop(
    lowcut,
    highcut,
    freq,
    order=4
):
    nyq = 0.5 * freq
    low = lowcut / nyq
    high = highcut / nyq
    b, a = butter(order, [low, high], btype='bandstop')
    return b, a

# применение фильтра пропускания полосы частот к сигналу
def butter_band_filter(
    data,
    lowcut,
    highcut,
    frequency,
    order=4,
):
    b, a = butter_band_pass(
        lowcut,
        highcut,
        frequency,
        order=order,
    )
    # Apply a digital filter forward
    res = filtfilt(
        b,
        a,
        data,
        method='gust',
        #method='pad',
        #padtype='odd',
        #padlen=3*(max(len(b),len(a))-1),
    )

    #
    # https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.lfilter.html
    return res

# ---------------------------------- AMPLITUTE, ARTEFACTS ----------------------
"""
ЭЭГ

    Низкая амплитуда
        Если средняя амплитуда за несколько секунд по любому из каналов оказывается менее 25 микровольт - размечаем это как артефакт, это по сути отсутствие содержательного сигнала

    Высокая амплитуда
        Если средняя амплитуда за несколько секунд по любому из каналов оказывается более 300 микровольт - размечаем это как артефакт.
        Если это наблюдается на определенных каналах и по длительности от 100 до 300 миллисекунд, то можно считать это артефактами моргания.

    Высокая амплитуда дельта ритма
        Фильтруем сигнал, оставляем от 0 до 3 Гц. Если амплитуда высока - размечаем как артефакт.

    Симметричные каналы
        Рассматриваем в комплексе симметричные каналы, например F3-F4, O1-O2, FP1-FP2, F7-F8, P3-P4. Симметричные - это четный + нечетный, но без центральных каналов Z.
        Если различие средних амплитуд за определенное время превышает 6 стандартных отклонений - размечаем для этого времени оба канала артефактными.

    Отбраковка негодных данных
        Если более трети времени на канале артефактны, канал считается негодным к дальнейшей обработке.
        Это отмечается в сводке каналов, прикрепляемой к данным (новое поле result_summary в формате JSON.
        В нем поле channel_summary - словарь, где ключ - имя канала, значение true если канал годный, false - если не годный).

Полиграф

    Анализ КГР
        У всех полиграфов только 1 канал КГР.
        Если за 3-5 секунд слишком малая или большая амплитуда, считаем, что в этот промежуток времени присутствует артефакт.
        Слишком малая это < 100 единиц вокруг среднего (200 суммарно)
        Слишком большая это > 200-250 единиц вокруг среднего (400-500 суммарно)

    Анализ ФПГ
        У полиграфа может быть 1 или 2 канала ФПГ. Каждый анализируем независимо.
        Фильтруем сигнал в диапазоне 0.5 - 2.5 Гц (это частоты пульса).
        Также смотрим на амплитуду, если она слишком мала - значит содержательного сигнала нет, такие участки помечаем артефактными.

    Отбраковка негодных данных
        Если более трети времени на канале артефактны, канал считается негодным к дальнейшей обработке.
        Это отмечается в сводке каналов, прикрепляемой к данным (новое поле result_summary в формате JSON.
        В нем поле channel_summary - словарь, где ключ - имя канала, значение true если канал годный, false - если не годный).
        Если КГР не годен или нет годного канала ФПГ - файл полиграфа считается не валидным, это отмечается в поле result_summary (тогда поле bad_file = true).
        Далее помеченные плохими данные не будут участвовать в автоматизированной обработке.
"""

# нормируем сигнал
def norm(x):
    ans = (x - np.mean(x, axis=0)) / np.std(x, axis=0)
    return ans

# средняя амплитуда для предоставленного отрезка данных
def mean_amplitude(data):
    if len(data) == 0:
        return 0

    mm = local_minmax(data)
    if len(mm) == 0:
        return 0

    summ = 0
    for num in mm:
        summ += abs(data[num])

    avg = summ / len(mm)
    return avg


# ф-я получает массив данных - действительных чисел, а также настройки
# на выход - массив значений амплитуды с заданной частотой дискретизации
# заодно находит артефакты при выходе амплитуды сигнала вне заданных рамок
def run_function(
    data,               # data, np.array
    data_freq,          # частота дискретизации данных
    function,           # amplitude, mean, std
    ans_freq=2,         # частота дискретизации ответа. По умолч 2
    dt=1000,            # промежуток времени, мс, за который брать амплитуду
    value_low=0,        # артефакты. Нижний порог отсечения, по умолчанию 25
    value_high=0,       # артефакты. Верхний порог отсечения, по умолчанию 300
):

    res = np.array([])
    art = np.array([])

    if not function or type(function) != str:
        print('empty function parameter')
        return -5, res, art

    function = function.lower()
    if function not in [
        'amplitude',
        'mean',
        'std',
    ]:
        print('incorrect function parameter')
        return -5, res, art

    data_len = data.size
    if data_len < 10:
        print('bad data')
        return -1, res, art
    if not data_freq or ans_freq > data_freq or ans_freq < 1:
        print('bad freq')
        return -2, res, art
    if dt < 100 or dt > 10000:
        print('bad dt')
        return -3, res, art

    all_time = data_len / data_freq
    ans_len = int(all_time * ans_freq)

    # величина шага
    step = data_freq / ans_freq
    if step < 1 or step > data_len:
        print('bad step %s' % step)
        return -4, res, art

    # столько отсчетов пройдет за dt миллисекунд
    dt_step = data_freq * dt / 1000

    print('function %s: data_len=%s all_time=%s  ans_len=%s  step=%s dt_step=%s' % (
        function, data_len, all_time, ans_len, step, dt_step,
    ))

    for i in range(ans_len):
        if step < dt_step:
            # шаг по герцовке меньше чем шаг по времени.
            # Размазываем шаг по герцовке вокруг шага по времени
            dstep = dt_step - step
            start = int(max(i * step - dstep / 2, 0))
            stop = int(min(start + dt_step, data_len))
        else:
            start = int(max(i * step, 0))
            stop = int(min(start + dt_step, data_len))

        dv = 0
        if function == 'amplitude':
            # вычисляем амплитуду
            arr = data[start:stop]
            dv = mean_amplitude(arr)
            #dv = np.max(arr) - np.min(arr)
            #if np.isnan(dv):
            #    dv = 0

        elif function == 'mean':
            # вычисляем среднее
            dv = np.mean(data[start:stop])

        elif function == 'std':
            # вычисляем std deviation
            dv = np.std(data[start:stop])

        res = np.append(res, dv)

        # вычисляем флаг артефакта для данного отрезка измерений
        is_art = 0
        if value_high != value_low:
            if dv < value_low or dv > value_high:
                is_art = 1
            art = np.append(art, is_art)

        #print('i=%s  start=%s stop=%s value=%s art=%s' % (i, start, stop, dv, is_art))

    return 0, res, art


#
def collect_artefacts(
    art,
    all_art=None,
):

    if len(all_art) == 0:
        all_art = np.copy(art)
    else:
        all_art = all_art + art

    return 0, all_art

# найти пики - вернет массив данных [(высота пика, номер измерения)]
def find_data_peaks(
    data,               # data, np.array
    data_freq,          # частота дискретизации данных
    peak_dt=0.05,       # временной радиус, за который окружающий пик данные должны падать
    height=50,          # требуемая абсолютная высота пиков
    prominence=50,      # насколько пики должны возвышаться над подножием (относительная высота)
    tresh=None,         # 
):
    num = np.array([])
    data_len = data.size
    if data_len < 10:
        print('bad data')
        return -1, num
    if not data_freq:
        print('bad freq')
        return -2, num
    if 0.5 < peak_dt <= 0:
        print('bad peak_dt')
        return -3, num

    peak_dc = data_freq * peak_dt

    print('find_data_peaks dlen={dl} freq={f} height={h} peak_dc={dc} tresh={t}'.format(
        dl=data_len,
        f=data_freq,
        h=height,
        dc=peak_dc,
        t=tresh,
    ))

    # find peaks https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.find_peaks.html
    try:
        num, prop = find_peaks(
            data,
            height=height,
            threshold=tresh,
            distance=peak_dc,
            prominence=prominence,
            width=peak_dc,
            wlen=None,
            rel_height=0.5,
            plateau_size=None,
        )
    except:
        exc_str = traceback.format_exc()
        print('Exception in find_peaks: %s' % exc_str)
        return -4, num

    return 0, num

# вычисляет разницу между мин и макс в скользящем по данным окне
def deltas(
    data,               # data, np.array
    data_freq,          # частота дискретизации данных
    window_size=9,      # размер скользящего окна
):
    ans = np.array([])
    data_len = data.size
    if data_len < 10:
        print('bad data')
        return -1, ans
    if not data_freq:
        print('bad freq')
        return -2, ans
    if window_size < 3 or window_size > 50:
        print('bad window size')
        return -3, ans

    #
    rad = window_size // 2
    for i in range(data_len):
        from_i = max(0, i - rad)
        to_i = min(data_len - 1, i + rad)
        arr = data[from_i:to_i]
        maxi = np.max(arr)
        mini = np.min(arr)
        ans = np.append(ans, maxi - mini)

    return 0, ans

# ------------------------------- SIGNAL POWER ---------------------------------
def count_power_for_diapazones(
    data,               # data, np.array
    frequency,          # частота дискретизации данных
    diapazones={},      # диапазоны данных, словарь. По ключам: {'low': *, 'high': *,}
    part_time=1,        # на куски какой длительности (в секундах) порубить данные
    window_size=5,      # окно какого размера (в секундах) использовать для каждого куска
):

    if len(diapazones) == 0:
        diapazones = {
            'all': {
                'low': 1,
                'high': 100,
            }
        }
    #print(diapazones)

    low = 1000
    high = 0
    if 'all' not in diapazones:
        for diap in diapazones:
            if diapazones[diap]['low'] < low:
                low = diapazones[diap]['low']
            if diapazones[diap]['high'] > high:
                high = diapazones[diap]['high']
        diapazones['all'] = {
            'low': low,
            'high': high,
        }
        #print(diapazones)

    part_len = part_time * frequency
    iters = len(data) // part_len
    print(part_len, len(data), iters)

    # empty power
    power = {}
    for diap in diapazones:
        power[diap] = np.array([])

    window_rad = window_size // 2
    for i in range(0, iters):

        if i < window_rad or i > iters - window_rad:
            # для этих индексов нет данных, пихаем заглушку
            freqs = np.arange(1, 256, 1)
            psd = np.zeros(255)

        else:
            # вычисляем спектр плотность сигнала для каждого участка по 1 сек
            dat = data[(i - window_rad) * part_len : (i + 1 + window_rad) * part_len]
            freqs, psd = signal.welch(
                dat,
                frequency,
                nperseg=part_len,
            )

        # Frequency resolution
        freq_res = freqs[1] - freqs[0]

        # для каждого спектр диапазона считаем мощность - интегрируем площадь под графиком
        for diap in diapazones:
            low = diapazones[diap]['low']
            high = diapazones[diap]['high']
            idx_delta = np.logical_and(freqs >= low, freqs <= high)

            i_power = simps(psd[idx_delta], dx=freq_res)
            power[diap] = np.append(power[diap], i_power)

    # считаем проценты, округляем
    for diap in diapazones:
        pwr = np.around(
            power[diap] / power['all'] * 100,
            decimals=2,
        )
        pwr[np.isnan(pwr)] = 0
        power['%s_p' % diap] = pwr

        power[diap] = np.around(
            power[diap],
            decimals=2,
        )

    return power


