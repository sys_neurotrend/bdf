#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
logger = logging.getLogger(__name__)
import pprint
pp = pprint.PrettyPrinter(indent=4)

import os
import sys
import json
from copy import deepcopy
import numpy as np

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

def graph(
    data,                       # данные основные которые рисовать
    data_freq,                  # частота дискретизации
    fname,                      # куда сохранить
    data2=np.array([]),         # данные дополнительные которые рисовать, необазятельно
    x_data=np.array([]),        # точки по Х. Если не заданы - делаются автоматически из частоты дискретизации
    points=np.array([]),        # нарисовать точки, здесь номера отсчетов
    points_color='red',         # цвет точек
    graph_color='blue',         # цвет графика
    graph2_color='green',       # цвет графика дополнительного
    sec_per_cm=2,               # сколько секунд уместить в сантиметр
    dpi=200,                    # разрешение, точек на дюйм
):
    dlen = len(data)
    plen = len(points)
    if dlen == 0 or data_freq == 0 or sec_per_cm == 0:
        return -1

    # больше 300 секунд не рисуем!
    seconds = dlen / data_freq
    if seconds > 300:
        seconds = 300
        dlen = int(seconds * data_freq)
        data = data[0:dlen]

        # вырезаем точки лежащие дальше 300 сек
        if plen > 0:
            i = 0
            while points[i] < dlen:
                i += 1
            points = points[0:i]
            plen = len(points)

    # считаем сколько сантиметров и дюймов длиной должен быть график
    cm = seconds / sec_per_cm
    inch_w = int(cm / 2.54)
    if inch_w < 5:
        inch_w = 5

    print('graph dlen={dl} sec={s} cm={cm} sec_per_cm={spc} inch={i} points_len={pl} color1={c1} color2={c2}'.format(
        dl=dlen,
        s=seconds,
        cm=cm,
        spc=sec_per_cm,
        i=inch_w,
        pl=plen,
        c1=graph_color,
        c2=graph2_color,
    ))
    if plen > 0:
        print('max_point=%s' % points[-1])

    # данные для оси х
    if len(x_data) == len(data):
        x = x_data
    else:
        # делаем данные для оси Х - секунды
        x = np.arange(0, seconds, 1 / data_freq)

    # данные для оси у
    y = np.resize(np.asarray(data), len(x))

    # get or create current figure and set size
    fig = matplotlib.pyplot.gcf()
    fig.set_size_inches(inch_w, 5, forward=True)

    # set axes https://stackoverflow.com/questions/8209568/how-do-i-draw-a-grid-onto-a-plot-in-python
    ax = fig.gca()
    xticks = np.arange(x[0], x[-1], (x[-1] - x[0]) / inch_w)
    ax.set_xticks(xticks)
    #ax.set_yticks(np.arange(0, 1., 0.1))

    # draw plot
    # https://matplotlib.org/3.2.1/api/_as_gen/matplotlib.pyplot.plot.html
    plot = plt.plot(
        x, y,
        color=graph_color,
        #marker='.',
        #linestyle='solid',
        #'.-g',          # green point markers with solid line style
        #alpha=0,
    )

    # рисуем доп данные
    if len(data2) > 0:
        # данные для оси у
        y = np.resize(np.asarray(data2), len(x))

        plot = plt.plot(
            x, y,
            color=graph2_color,
            #marker='.',
            #linestyle='solid',
            #'.-g',          # green point markers with solid line style
            #alpha=0,
        )

    # если есть точки - рисуем их
    if plen > 0:
        vals = np.array([])
        ps = np.array([])
        for p in np.nditer(points):
            vals = np.append(vals, y[p])
            ps = np.append(ps, p / data_freq)

        plt.scatter(
            ps, vals,
            color=points_color,
            # ('o', 'v', '^', '<', '>', '8', 's', 'p', '*', 'h', 'H', 'D', 'd', 'P', 'X')
            marker='o',
            #alpha=0.5,
        )

    # set grid
    plt.grid()

    fig.savefig(fname, dpi=dpi)
    plt.close(fig)
    return 0






