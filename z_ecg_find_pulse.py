#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
logger = logging.getLogger(__name__)
import pprint
pp = pprint.PrettyPrinter(indent=4)

import os
import sys
import csv
import json
import codecs
from copy import deepcopy
import numpy as np
import heartpy as hp
import scipy.signal as signal

from edf.edf import EDF
from edf.plot import graph
from edf.functions import (
    local_minmax,
    local_min,
    local_max,
    butter_band_filter,
    run_function,
    resampling,
    collect_artefacts,
    find_data_peaks,
)

OUT_DIVISOR = '\t'
PARTS_COUNT = 3         # на сколько частей побить запись

#fname = "samples/eeg_ntrend_eeg_21_2.bdf"
#fname = "samples/EEG---C56.EDF"
fname = "samples/C56.bdf"

dout = "samples/"
fout = "samples/out.bdf"


e = EDF()

res = e.open_file('debug/debug.log', 'w', 'debug')
if res != 0:
    print('Cant open debug file')
    exit()

res = e.open_file(
    fname=fname,
    right='r',
    typ='header',
)
if res != 0:
    print(e.error)
    exit()
print("Open header - OK!")

#pp.pprint(e.last_result)

res = e.open_file(
    fname,
    'r',
    'data',
)
if res != 0:
    print(e.error)
    exit()
print("Read data - OK!")

in_edf = e.files['data']
inparams = in_edf['params']
canal_num = inparams['canal_names'].get('ECG')
if canal_num is None:
    print('Cant find number for canal name = {cn}'.format(cn=canal_name))
    exit()

print('Canal num = %s' % canal_num)

summary = deepcopy(inparams['canals'][canal_num])
frequency = summary['frequency']
#sample_rate = 250

res = e.get_channel_data(
    typ='data',
    number=canal_num,
    #name='ECG',
)
if res != 0:
    print(e.error)
    exit()
print("Get channel - OK!")

all_data = e.last_result
print('All data len=%s' % len(all_data))

#
for part in range(PARTS_COUNT):
    data_start = int(part / PARTS_COUNT * len(all_data))
    data_finish = int((part + 1) / PARTS_COUNT * len(all_data)) - 1
    data = all_data[data_start : data_finish]
    print('Work part=%s start=%s finish=%s len=%s' % (
        part + 1,
        data_start,
        data_finish,
        len(data)
    ))

    res = graph(
        data=data,
        data_freq=frequency,
        fname=dout + 'DATA__part_%s.png' % (part + 1)
    )

    # filter
    order = 4
    try:
        f_data = butter_band_filter(
            data=data,
            lowcut=0.2,
            highcut=3,
            frequency=frequency,
            order=order,
        )

    except:
        exc_str = traceback.format_exc()
        mess = "Exception: {err}".format(err=exc_str)
        print(mess)
        exit()

    res = graph(
        data=f_data,
        data_freq=frequency,
        fname=dout + 'DATA_filt__part_%s.png' % (part + 1)
    )

    #"""
    # все что улетает за thresh приводим к этому интервалу
    thresh = 200
    low_i = f_data < -thresh
    f_data[low_i] = -thresh
    high_i = f_data > thresh
    f_data[high_i] = thresh

    res = graph(
        data=f_data,
        data_freq=frequency,
        fname=dout + 'DATA_filt_cut__part_%s.png' % (part + 1)
    )
    #"""

    res, num = find_data_peaks(
        data=f_data,
        data_freq=frequency,
        height=1,
        peak_dt=0.1,
    )
    if res != 0:
        print('error peaks')
        exit()

    res = graph(
        data=f_data,
        data_freq=frequency,
        fname=dout + 'DATA_peaks__part_%s.png' % (part + 1),
        points=num,
    )

    """
    minmax = local_minmax(f_data)
    res = graph(
        data=f_data,
        data_freq=frequency,
        fname=dout + 'DATA_minmax__part_%s.png' % (part + 1),
        points=minmax,
    )

    dmin = local_min(f_data)
    res = graph(
        data=f_data,
        data_freq=frequency,
        fname=dout + 'DATA_min__part_%s.png' % (part + 1),
        points=dmin,
    )

    dmax = local_max(f_data)
    res = graph(
        data=f_data,
        data_freq=frequency,
        fname=dout + 'DATA_max__part_%s.png' % (part + 1),
        points=dmax,
    )
    """

    res, amp, art = run_function(
        data=f_data,
        data_freq=frequency,
        function='amplitude',
        ans_freq=2,
        dt=3000,
        value_low=10,
        value_high=800,
    )

    # размножаем данные для амплитуды, чтоб вышли в той же частоте
    freq_div = int(frequency / 2)
    data2 = np.repeat(amp, freq_div)

    res = graph(
        data=f_data,
        data2=data2,
        data_freq=frequency,
        fname=dout + 'DATA_ampl__part_%s.png' % (part + 1),
    )

    # ------------------------- periodogram for dt -----------------------------
    # vals
    #y = data[0 : time_s * frequency]
    lt = 0
    t = np.array([])
    dt = np.array([])
    rows = []
    for p in np.nditer(num):
        ti = p / frequency
        t = np.append(t, ti)
        dt = np.append(dt, ti - lt)

        # for csv out
        dat = {
            'frame_num': p,
            'time': int(ti * 1000),
            'dt': int(1000 * (ti - lt)),
            'value': f_data[p],
        }
        rows.append(dat)

        lt = ti

    # sample times
    time_s = 2
    x = np.linspace(0, time_s, len(dt))
    #print(x)

    """
    # частоты по которым строить периодограмму - равномерно возрастает от минимума до максимума
    out_fr = 1000       # количество отсчетов при возрастании на 1 гц
    from_fr = 0.001     #
    to_fr = 0.4         # до какой частоты наши данные анализировать
    f = np.linspace(from_fr, to_fr, out_fr * to_fr)

    #pgram = signal.lombscargle(x, dt, f, normalize=True)
    pgram = signal.lombscargle(t, dt, f, normalize=True)
    res = graph(
        data=pgram,
        x_data=f,
        data_freq=out_fr,
        fname=dout + 'DATA_001-400_part_%s.png' % (part + 1),
        sec_per_cm=1,
    )

    fout = dout + 'lombscarge_part_%s.csv' % (part + 1)
    with open(fout, 'w', newline='') as csvfile:
        fieldnames = ['freq', 'power']
        writer = csv.DictWriter(
            csvfile,
            fieldnames=fieldnames,
            delimiter=OUT_DIVISOR,
        )
        writer.writeheader()

        for i in range(len(f)):
            dat = {
                'freq': f[i],
                'power': int(pgram[i] * 10000) / 10000,
            }
            writer.writerow(dat)
    """

    # пишем csv
    fout = dout + 'pulse_part_%s.csv' % (part + 1)
    with open(fout, 'w', newline='') as csvfile:
        fieldnames = ['time', 'dt', 'frame_num', 'value']
        writer = csv.DictWriter(
            csvfile,
            fieldnames=fieldnames,
            delimiter=OUT_DIVISOR,
        )
        writer.writeheader()

        for r in rows:
            writer.writerow(r)


exit()



