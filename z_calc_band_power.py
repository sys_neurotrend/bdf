#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
logger = logging.getLogger(__name__)
import pprint
pp = pprint.PrettyPrinter(indent=4)

import os
import sys
import csv
from copy import deepcopy
from collections import OrderedDict
import numpy as np
import scipy.signal as signal
from scipy.integrate import simps

from edf.edf import EDF
from edf.plot import graph
from edf.functions import (
    butter_band_filter,
    count_power_for_diapazones,
)

OUT_DIVISOR = '\t'
PARTS_COUNT = 3         # на сколько частей побить запись

fname = "samples/eeg_ntrend_eeg_21_2.bdf"

dout = "samples/"
fout = "samples/out.bdf"

eeg_diapazones = OrderedDict()
eeg_diapazones['all'] = {'low': 4, 'high': 30,}
eeg_diapazones['TH'] = {'low': 4, 'high': 8,}
eeg_diapazones['A1'] = {'low': 8, 'high': 10,}
eeg_diapazones['A2'] = {'low': 10, 'high': 12,}
eeg_diapazones['B1'] = {'low': 13, 'high': 20,}
eeg_diapazones['B2'] = {'low': 20, 'high': 30,}

e = EDF()

res = e.open_file('debug/debug.log', 'w', 'debug')
if res != 0:
    print('Cant open debug file')
    exit()

res = e.open_file(
    fname=fname,
    right='r',
    typ='header',
)
if res != 0:
    print(e.error)
    exit()
print("Open header - OK!")

#pp.pprint(e.last_result)

res = e.open_file(
    fname,
    'r',
    'data',
)
if res != 0:
    print(e.error)
    exit()
print("Read data - OK!")

in_edf = e.files['data']
inparams = in_edf['params']
canal_name = 'Cz'
canal_num = inparams['canal_names'].get(canal_name)
if canal_num is None:
    print('Cant find number for canal name = {cn}'.format(cn=canal_name))
    exit()
print('Canal num = %s' % canal_num)

summary = deepcopy(inparams['canals'][canal_num])
frequency = summary['frequency']

res = e.get_channel_data(
    typ='data',
    number=canal_num,
)
if res != 0:
    print(e.error)
    exit()
print("Get channel - OK!")

all_data = e.last_result
print('All data len=%s' % len(all_data))

#
for part in range(PARTS_COUNT):
    data_start = int(part / PARTS_COUNT * len(all_data))
    data_finish = int((part + 1) / PARTS_COUNT * len(all_data)) - 1
    data = all_data[data_start : data_finish]
    print('Work part=%s start=%s finish=%s len=%s' % (
        part + 1,
        data_start,
        data_finish,
        len(data)
    ))

    """
    low = 1
    high = 35
    order = 2
    data = butter_band_filter(
        data=data,
        lowcut=low,
        highcut=high,
        frequency=frequency,
        order=order,
    )
    """

    """
    res = graph(
        data=data,
        data_freq=frequency,
        sec_per_cm=1,
        fname=dout + 'DATA__part_%s.png' % (part + 1)
    )
    """

    # обрабатываем каждый частотный диапазон
    for diap in eeg_diapazones:
        # выделяем частотный диапазон
        low = eeg_diapazones[diap]['low']
        high = eeg_diapazones[diap]['high']
        order = 2
        flabel = '{label}_{diap}'.format(
            label=canal_name,
            diap=diap,
        )
        print("Add filtered channel %s" % flabel)

        try:
            f_data = butter_band_filter(
                data=data,
                lowcut=low,
                highcut=high,
                frequency=frequency,
                order=order,
            )

        except:
            exc_str = traceback.format_exc()
            mess = "Exception: {err}".format(err=exc_str)
            print(mess)
            continue

        """
        res = graph(
            data=f_data,
            data_freq=frequency,
            sec_per_cm=1,
            fname=dout + 'DATA__part_%s__filt_%s.png' % (part + 1, diap)
        )
        """

    power = count_power_for_diapazones(
        data=data,
        frequency=frequency,
        diapazones=eeg_diapazones,
        part_time=1,
        window_size=9,
    )
    """
    for diap in eeg_diapazones:
        print(diap)
        print(power[diap][0:30])
        print(power['%s_p' % diap][0:30])
    """

    std = np.std(power['all'])
    avg = np.mean(power['all'])
    print(avg, std)

    arts = np.array([])
    i = 0
    for p in power['all']:
        art = 0
        if avg < 1 or std < 1 or p < 1:
            art = 1
            print('i=%s Art 1 avg=%s std=%s p=%s' % (i, avg, std, p))
        elif abs(p) > abs(avg) + 2 * std:
            art = 1
            print('i=%s Art 2 avg=%s std=%s p=%s' % (i, avg, std, p))

        arts = np.append(arts, art)
        i += 1

    #print(power['all'][0:50])
    #print(arts[0:50])

exit()


