#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
logger = logging.getLogger(__name__)

import pprint
pp = pprint.PrettyPrinter(indent=4)

import os
import sys
import json
import codecs
import numpy as np
from edf.edf import EDF
from copy import deepcopy

#fname = "samples/GarantEEG.bdf"
#fname = "samples/BAlertEEG.edf"                # не читает корректно надо доделывать!!!!
fname = "samples/NTrendEEG21.bdf"
dout = "samples/"
fout = "samples/out.bdf"


#изменение частоты данных
def resampling(a, current_freq, new_freq):
    current_size = len(a)
    if not current_size or not current_freq:
        mess = 'Invalid data in file while resampling frequency - zero array length or zero current frequency'
        raise Exception(mess)

    coeff = new_freq / current_freq
    new_size = int(current_size * coeff)
    new_arr = np.zeros(new_size)

    for i in range(0, new_size):
        old_index = int(i / coeff)
        new_arr[i] = a[old_index]

    return new_arr


e = EDF()

res = e.open_file('debug/debug.log', 'w', 'debug')
if res != 0:
    print('Cant open debug file')
    exit()

res = e.open_file(
    fname=fname,
    right='r',
    typ='header',
)
if res != 0:
    e.debug(e.error)
    exit()
e.debug("Open header - OK!")

pp.pprint(e.last_result)

"""
переформировка NTrend EEG21
Порядок каналов для устройства "NTrend EEG21" (250 гц):
    0   Fp1     -> Fp1  +
    1   F7      -> F7   +
    2   F3      -> F8   +
    3   Fz      -> T4   +
    4   T3      -> T6   +
    5   C3      -> T5   +
    6   T5      -> T3   +
    7   P3      -> Fp2  +
    8   Pz      -> O1   +
    9   O1      -> P3   +
    10  Fp2     -> Pz   +
    11  F8      -> F3   +
    12  F4      -> Fz   +
    13  T4      -> F4   +
    14  C4      -> C4   +
    15  Cz      -> P4   +
    16  T6      -> POz  -       Pz
    17  P4      -> C3   +
    18  O2      -> Cz   +
    19  A2      -> O2   +
    20          -> Annot

    Ниже не используется нейросетью
"""

# read all data
need_canals = list(range(0, 21))
res = e.open_file(
    fname=fname,
    right='r',
    typ='data',
    offset=0,
    cnt=0,
    need_canals=need_canals,
)
if res != 0:
    print(e.error)
    exit()
print("Read data - OK!")

res = e.open_file(fout, 'w', 'out')
if res != 0:
    print(e.error)
    exit()

print("Open out - OK!")

in_edf = e.files['data']
inparams = in_edf['params']
sc = inparams['signal_count']
duration = inparams['duration']

out_edf = e.files['out']
out_edf['params']['duration'] = duration

canal_names = [
    'Fp1',
    'F7',
    'F8',
    'T4',
    'T6',
    'T5',
    'T3',
    'Fp2',
    'O1',
    'P3',
    'Pz',
    'F3',
    'Fz',
    'F4',
    'C4',
    'P4',
    'Pz',                       # вместо POz
    'C3',
    'Cz',
    'O2',
    #'BDF Annotations',         # пока не понятно как стд средствами правильно писать канал аннотаций
]

# добавляем по очереди все нужные каналы в описание выходного файла
for canal_name in canal_names:
    print('Try add channel %s' % canal_name)

    canal_num = inparams['canal_names'].get(canal_name)
    if canal_num is None:
        print('Cant find number for canal name = {cn}'.format(cn=canal_name))
        exit()

    print('Canal num = %s' % canal_num)

    # готовим данные по каждому каналу
    summary = deepcopy(inparams['canals'][canal_num])
    summary['frequency'] = 256

    # ресамплим в другую частоту, с 250 до 256
    data = np.asarray(inparams['canal_data'][canal_num])
    new_data = resampling(data, 250, 256)

    # перефигачиваем их в выходной файл
    res = e.out_add_channel(summary, new_data)
    if res != 0:
        print(e.error)
        exit()

    print("Add channel - OK!\n")

# сохраняем выходной файл
res = e.out_save()
if res != 0:
    print(e.error)
    exit()

print("Save - OK!")


