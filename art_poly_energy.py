#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
logger = logging.getLogger(__name__)

import pprint
pp = pprint.PrettyPrinter(indent=4)

import os
import sys
import json
import codecs
import traceback
import numpy as np
from edf.edf import EDF
from copy import deepcopy
from edf.functions import (
    butter_band_filter,
    run_function,
    resampling,
    collect_artefacts,
)

order = 2
amp_freq = 2
dt = 3000

din = "samples/"
fin = 'poly_energy.dat'
fname = din + fin

dout = "samples/"
fout = dout + "amp_art_{fin}.bdf".format(
    fin=fin,
)

e = EDF()

res = e.open_file('debug/debug.log', 'w', 'debug')
if res != 0:
    print('Cant open debug file')
    exit()

res = e.open_file(
    fname=fname,
    right='r',
    typ='energy',
)
if res != 0:
    print(e.error)
    exit()
print("Open energy - OK!")

pp.pprint(e.last_result)

res = e.open_file(fout, 'w', 'out')
if res != 0:
    print(e.error)
    exit()

print("Open out - OK!")

in_edf = e.files['energy']
inparams = in_edf['params']
sc = inparams['signal_count']

out_edf = e.files['out']

# energy - all 100 Hz
canal_names = [
    'SGR',
    'PPG1',
    'ACC1',
    'ACC2',
    'MOV',
    'PPG2',
]

# добавляем по очереди все нужные каналы в выходн файл
all_art = None
for canal_name in canal_names:

    canal_num = inparams['canal_names'].get(canal_name)
    if canal_num is None:
        print('Cant find number for canal name = {cn}'.format(cn=canal_name))
        exit()

    print('Canal num = %s' % canal_num)

    # готовим данные по каждому каналу
    summary = deepcopy(inparams['canals'][canal_num])
    data = np.asarray(inparams['canal_data'][canal_num])
    sample_rate = summary['sample_rate']
    frequency = summary['frequency']
    label = summary['label']

    # перефигачиваем их в выходной файл
    res = e.out_add_channel(summary, data)
    if res != 0:
        print(e.error)
        exit()

    print("Add channel - OK!\n")


    # фильтруем
    if canal_name in ['PPG1', 'PPG2']:
        low_filter = 0.5
        high_filter = 2.5
        flabel = '{label}_f'.format(
            label=label,
        )
        print("Add filtered channel %s" % flabel)

        try:
            f_data = butter_band_filter(
                data=data,
                lowcut=low_filter,
                highcut=high_filter,
                frequency=frequency,
                order=order,
            )

        except:
            exc_str = traceback.format_exc()
            mess = "Exception: {err}".format(err=exc_str)
            print(mess)
            continue

        # перефигачиваем в выходной файл
        new_summary = deepcopy(summary)
        new_summary['label'] = flabel
        new_summary['frequency'] = frequency
        res = e.out_add_channel(new_summary, f_data)
        if res != 0:
            print(e.error)
            exit()

        print("Add channel %s - OK!\n" % flabel)

    else:
        # SGR не фильтруем
        f_data = deepcopy(data)


    if canal_name not in ['SGR', 'PPG1', 'PPG2']:
        continue

    # считаем амплитуду
    flabel = '{label}_amp'.format(
        label=label,
    )
    print("Add amplitude channel=%s canal_name=%s" % (flabel, canal_name))

    if canal_name in ['SGR', 'SCR']:
        """
        У всех полиграфов только 1 канал КГР.
        Если за 3-5 секунд слишком малая или большая амплитуда, считаем, что в этот промежуток времени присутствует артефакт.
        Слишком малая это < 100 единиц вокруг среднего (200 суммарно)
        Слишком большая это > 200-250 единиц вокруг среднего (400-500 суммарно)
        """
        res, amp, art = run_function(
            data=f_data,
            data_freq=frequency,
            function='amplitude',
            ans_freq=amp_freq,
            dt=dt,
            value_low=10,
            value_high=200,
        )

    elif canal_name in ['PPG1', 'PPG2']:
        """
        У полиграфа может быть 1 или 2 канала ФПГ. Каждый анализируем независимо.
        Фильтруем сигнал в диапазоне 0.5 - 2.5 Гц (это частоты пульса).
        Также смотрим на амплитуду, если она слишком мала - значит содержательного сигнала нет, такие участки помечаем артефактными.
        """
        res, amp, art = run_function(
            data=f_data,
            data_freq=frequency,
            function='amplitude',
            ans_freq=amp_freq,
            dt=dt,
            value_low=10,
            value_high=800,
        )

    if res != 0:
        print('Error calculating amplitude!')
        exit()

    # перефигачиваем амплитуду в выходной файл
    new_summary = deepcopy(summary)
    new_summary['label'] = flabel
    new_summary['frequency'] = amp_freq
    print(new_summary)

    res = e.out_add_channel(new_summary, amp)
    if res != 0:
        print(e.error)
        exit()

    print("Add channel %s - OK!\n" % flabel)


    # канал артефактов
    alabel = '{label}_art'.format(
        label=label,
    )
    print("Add amplitude channel %s" % alabel)

    # перефигачиваем артефакты в выходной файл
    new_summary = deepcopy(summary)
    new_summary['label'] = alabel
    new_summary['frequency'] = amp_freq
    new_summary['physical_max'] = 1
    new_summary['physical_min'] = 0
    new_summary['dimension'] = 'art'
    print(new_summary)

    res = e.out_add_channel(new_summary, art)
    if res != 0:
        print(e.error)
        exit()

    print("Add channel %s - OK!\n" % flabel)

    # сливаем все артефакты в одну кучу
    res, all_art = collect_artefacts(art, all_art)
    if res != 0:
        print('Error collection artefacts!')
        exit()

# пишем итоговый канал артефактов
new_summary = deepcopy(summary)
alabel = 'ALL_ART'
new_summary['label'] = alabel
new_summary['frequency'] = amp_freq
new_summary['samples'] = amp_freq
new_summary['sample_rate'] = amp_freq
new_summary['physical_max'] = 3
new_summary['physical_min'] = 0
new_summary['dimension'] = 'art'
print(new_summary)
print(all_art[:40])

res = e.out_add_channel(new_summary, all_art)
if res != 0:
    print(e.error)
    exit()

print("Add channel %s - OK!\n" % alabel)


# сохраняем выходной файл
res = e.out_save()
if res != 0:
    print(e.error)
    exit()

print("Save - OK!")



