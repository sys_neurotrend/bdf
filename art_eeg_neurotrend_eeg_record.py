#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
logger = logging.getLogger(__name__)

import pprint
pp = pprint.PrettyPrinter(indent=4)

import os
import sys
import json
import codecs
import traceback
import numpy as np
from edf.edf import EDF
from edf.functions import (
    butter_band_filter,
    run_function,
    resampling,
    collect_artefacts,
)
from copy import deepcopy

low_filter = 0.5
high_filter = 5
order = 4
amp_freq = 2
dt = 3000

din = "samples/"
fin = 'eeg_neurotrend_eeg_record.edf'
fname = din + fin

dout = "samples/"
fout = dout + "amp_art_{fin}".format(
    fin=fin,
    low=low_filter,
    high=high_filter,
)

e = EDF()

res = e.open_file('debug/debug.log', 'w', 'debug')
if res != 0:
    print('Cant open debug file')
    exit()

res = e.open_file(
    fname=fname,
    right='r',
    typ='header',
)
if res != 0:
    e.debug(e.error)
    exit()
e.debug("Open header - OK!")

pp.pprint(e.last_result)

# read all data
need_canals = list(range(0, 21))
res = e.open_file(
    fname=fname,
    right='r',
    typ='data',
    offset=0,
    cnt=0,
    need_canals=need_canals,
)
if res != 0:
    print(e.error)
    exit()
print("Read data - OK!")

res = e.open_file(fout, 'w', 'out')
if res != 0:
    print(e.error)
    exit()
print("Open out - OK!")

in_edf = e.files['data']
inparams = in_edf['params']
sc = inparams['signal_count']
duration = inparams['duration']

out_edf = e.files['out']
out_edf['params']['duration'] = duration

"""
Порядок каналов для устройства "Neurotrend EEG record" (256 гц) - эталон:
    0   Fp1
    1   F7
    2   F8
    3   T4
    4   T6
    5   T5
    6   T3
    7   Fp2
    8   O1
    9   P3
    10  Pz
    11  F3
    12  Fz
    13  F4
    14  C4
    15  P4
    16  POz
    17  C3
    18  Cz
    19  O2
"""
canal_names = [
    'Fp1',
    'F7',
    'F8',
    'T4',
    'T6',
    'T5',
    'T3',
    'Fp2',
    'O1',
    'P3',
    'Pz',
    'F3',
    'Fz',
    'F4',
    'C4',
    'P4',
    'POz',
    'C3',
    'Cz',
    'O2',
    #'BDF Annotations',         # пока не понятно как стд средствами правильно писать канал аннотаций
]

# добавляем по очереди все нужные каналы в выходн файл
all_art = None
for canal_name in canal_names:

    canal_num = inparams['canal_names'].get(canal_name)
    if canal_num is None:
        print('Cant find number for canal name = {cn}'.format(cn=canal_name))
        exit()

    print('Canal num = %s' % canal_num)

    summary = deepcopy(inparams['canals'][canal_num])
    data = np.asarray(inparams['canal_data'][canal_num])
    sample_rate = summary['sample_rate']
    frequency = summary['frequency']
    label = summary['label']

    print('work channel=%s sample_rate=%s freq=%s' % (label, sample_rate, frequency))


    # добавляем исх канал
    res = e.out_add_channel(summary, data)
    if res != 0:
        print(e.error)
        exit()
    print("Add channel %s - OK!\n" % label)


    # фильтруем
    flabel = '{label}_f_{low}_{high}'.format(
        label=label,
        low=low_filter,
        high=high_filter,
    )
    print("Add filtered channel %s" % flabel)

    try:
        f_data = butter_band_filter(
            data=data,
            lowcut=low_filter,
            highcut=high_filter,
            frequency=frequency,
            order=order,
        )

    except:
        exc_str = traceback.format_exc()
        mess = "Exception: {err}".format(err=exc_str)
        print(mess)
        continue

    # перефигачиваем в выходной файл
    new_summary = deepcopy(summary)
    new_summary['label'] = flabel
    res = e.out_add_channel(new_summary, f_data)
    if res != 0:
        print(e.error)
        exit()

    print("Add channel %s - OK!\n" % flabel)


    # считаем амплитуду
    flabel = '{label}_amp_{atime}_{dt}'.format(
        label=label,
        atime=amp_freq,
        dt=int(dt/1000),
    )
    print("Add amplitude channel %s" % flabel)

    res, amp, art = run_function(
        data=f_data,
        data_freq=frequency,
        function='amplitude',
        ans_freq=amp_freq,
        dt=dt,
        value_low=20,
        value_high=300,
    )
    if res != 0:
        print('Error calculating amplitude!')
        exit()

    # перефигачиваем амплитуду в выходной файл
    new_summary = deepcopy(summary)
    new_summary['label'] = flabel
    new_summary['frequency'] = amp_freq
    print(new_summary)

    res = e.out_add_channel(new_summary, amp)
    if res != 0:
        print(e.error)
        exit()

    print("Add channel %s - OK!\n" % flabel)


    # канал артефактов
    alabel = '{label}_art_{atime}_{dt}'.format(
        label=label,
        atime=amp_freq,
        dt=int(dt/1000),
    )
    print("Add amplitude channel %s" % alabel)

    # перефигачиваем артефакты в выходной файл
    new_summary = deepcopy(summary)
    new_summary['label'] = alabel
    new_summary['frequency'] = amp_freq
    new_summary['physical_max'] = 1
    new_summary['physical_min'] = 0
    new_summary['dimension'] = 'art'
    print(new_summary)

    res = e.out_add_channel(new_summary, art)
    if res != 0:
        print(e.error)
        exit()

    print("Add channel %s - OK!\n" % flabel)

    # сливаем все артефакты в одну кучу
    res, all_art = collect_artefacts(art, all_art)
    if res != 0:
        print('Error collection artefacts!')
        exit()

# пишем итоговый канал артефактов
new_summary = deepcopy(summary)
alabel = 'ALL_ART'
new_summary['label'] = alabel
new_summary['frequency'] = amp_freq
new_summary['samples'] = amp_freq
new_summary['sample_rate'] = amp_freq
new_summary['physical_max'] = len(canal_names)
new_summary['physical_min'] = 0
new_summary['dimension'] = 'art'
print(new_summary)
print(all_art[:40])

res = e.out_add_channel(new_summary, all_art)
if res != 0:
    print(e.error)
    exit()

print("Add channel %s - OK!\n" % alabel)


# сохраняем выходной файл
res = e.out_save()
if res != 0:
    print(e.error)
    exit()

print("Save - OK!")





exit()
