#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
logger = logging.getLogger(__name__)
import pprint
pp = pprint.PrettyPrinter(indent=4)

import os
import sys
import json
import codecs
from copy import deepcopy
import numpy as np
import heartpy as hp

from edf.edf import EDF
from edf.plot import graph
from edf.functions import (
    butter_band_filter,
    resampling,
    collect_artefacts,
    find_data_peaks,
)

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
plt.rcParams["figure.figsize"] = (16, 9)

#fname = "samples/GarantEEG.bdf"
#fname = "samples/BAlertEEG.edf"                # не читает корректно надо доделывать!!!!
#fname = "samples/NTrendEEG21.bdf"

#fname = "samples/poly_ntrend_wirstband_3.bdf"
#fname = "samples/eeg_ntrend_eeg_21_2.bdf"
fname = "samples/EEG---C56.EDF"

dout = "samples/"
fout = "samples/out.bdf"

m = np.array([1, 2, 3, 5.5])
mm = np.repeat(m, 2)
print(mm)
exit()

e = EDF()

res = e.open_file('debug/debug.log', 'w', 'debug')
if res != 0:
    print('Cant open debug file')
    exit()

res = e.open_file(
    fname=fname,
    right='r',
    typ='header',
)
if res != 0:
    print(e.error)
    exit()
print("Open header - OK!")

res = e.open_file(
    fname,
    'r',
    'data',
)
if res != 0:
    print(e.error)
    exit()
print("Read data - OK!")

pp.pprint(e.files['data']['params']['canals'])
pp.pprint(e.files['data']['params']['canal_names'])

res = e.delete_channel(
    typ='data',
    #number=6,
    #name='BDF Annotations',
    name='ACC_X',
)
if res != 0:
    print(e.error)
    exit()
print("Delete channel - OK!")

pp.pprint(e.files['data']['params']['canals'])
pp.pprint(e.files['data']['params']['canal_names'])



exit()



res = e.open_file(
    fname,
    'r',
    'data',
    offset=0,
    cnt=2,
)
if res != 0:
    e.debug(e.error)
    exit()
e.debug("Read data - OK!")

#pp.pprint(e.files['data']['params']['canal_data'][52])

res = e.open_file(fname, 'r', 'data', offset=0, cnt=8)
if res != 0:
    e.debug(e.error)
    exit()
e.debug("Read data - OK!")

#pp.pprint(e.files['data']['params']['canal_data'][52])

res = e.open_file(fname, 'r', 'data', offset=2, cnt=2)
if res != 0:
    e.debug(e.error)
    exit()
e.debug("Read data - OK!")

#pp.pprint(e.files['data']['params']['canal_data'][52])


exit()



# стандартное открытие, добавление канала, запись
res = e.open_file(fname, 'r', 'in')
if res != 0:
    e.debug(e.error)
    exit()
e.debug("Open in - OK!")

res = e.open_file(fout, 'w', 'out')
if res != 0:
    e.debug(e.error)
    exit()
e.debug("Open out - OK!")

edf = e.files['in']
params = edf['params']
sc = params['signal_count']
duration = params['duration']
e.files['out']['params']['duration'] = duration

for c in range(sc):
    # готовим данные по каждому каналу
    summary = params['canals'][c]
    e.debug(summary)

    res = e.read_signal(c)
    if res != 0:
        e.debug(e.error)
        exit()
    data = np.asarray(e.result)

    # перефигачиваем их в выходной файл
    res = e.out_add_channel(summary, data)
    if res != 0:
        e.debug(e.error)
        exit()
    e.debug("Add channel - OK!")

# add 1 more channel
summary = {
    'label': 'squarewave',
    'dimension': 'uV',
    'sample_rate': 200,
    'physical_max': 100,
    'physical_min': -100,
    'digital_max': 32767,
    'digital_min': -32768,
    'transducer': '',
    'prefilter':''
}

time = np.linspace(0, duration, duration*200)
xtemp = np.sin(2*np.pi*0.1*time)
x1 = xtemp.copy()
x1[np.where(xtemp > 0)[0]] = 100
x1[np.where(xtemp < 0)[0]] = -100

res = e.out_add_channel(summary, x1)
if res != 0:
    e.debug(e.error)
    exit()
e.debug("Add channel - OK!")

# сохраняем выходной файл
res = e.out_save()
if res != 0:
    e.debug(e.error)
    exit()
e.debug("Save - OK!")


