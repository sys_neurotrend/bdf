#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
logger = logging.getLogger(__name__)
import pprint
pp = pprint.PrettyPrinter(indent=4)

import os
import sys
import csv
import json
import codecs
from copy import deepcopy
import numpy as np
import heartpy as hp
import scipy.signal as signal

from edf.edf import EDF
from edf.plot import graph
from edf.functions import (
    local_minmax,
    local_min,
    local_max,
    butter_band_filter,
    run_function,
    resampling,
    collect_artefacts,
    find_data_peaks,
    deltas,
)

"""
wband http://62.205.181.186:30003/admin/core/data/11270/change/?
ppg http://62.205.181.186:30003/admin/core/data/11271/change/?
pulse http://62.205.181.186:30003/admin/core/data/11299/change/?
"""

# truncating number
def round(n, power=0):
    if power > 6:
        power = 6
    p10 = 10 ** power
    return int(n * p10) / p10


OUT_DIVISOR = '\t'
PARTS_COUNT = 1         # на сколько частей побить запись
DO_GRAPH = 1
SIGMA_FACTOR = 2

fname = "samples/poly_ppg.bdf"
dout = "samples/"
fout = "samples/out.bdf"


e = EDF()

res = e.open_file('debug/debug.log', 'w', 'debug')
if res != 0:
    print('Cant open debug file')
    exit()

res = e.open_file(
    fname=fname,
    right='r',
    typ='header',
)
if res != 0:
    print(e.error)
    exit()
print("Open header - OK!")

#pp.pprint(e.last_result)

res = e.open_file(
    fname,
    'r',
    'data',
)
if res != 0:
    print(e.error)
    exit()
print("Read data - OK!")

in_edf = e.files['data']
inparams = in_edf['params']
canal_num = inparams['canal_names'].get('PPG')
if canal_num is None:
    print('Cant find number for canal name = {cn}'.format(cn=canal_name))
    exit()

print('Canal num = %s' % canal_num)

summary = deepcopy(inparams['canals'][canal_num])
frequency = summary['frequency']

res = e.get_channel_data(
    typ='data',
    number=canal_num,
)
if res != 0:
    print(e.error)
    exit()
print("Get channel - OK!")

all_data = e.last_result
print('All data len=%s' % len(all_data))

"""
Масштабируем данные
"""
avgp = np.mean(all_data[all_data > 0])
factor = 300 / avgp
all_data = all_data * factor
print('avg_plus=%s factor=%s' % (avgp, factor))

#
for part in range(PARTS_COUNT):
    data_start = int(part / PARTS_COUNT * len(all_data))
    data_finish = int((part + 1) / PARTS_COUNT * len(all_data)) - 1
    data = all_data[data_start : data_finish]
    print('Work part=%s start=%s finish=%s len=%s' % (
        part + 1,
        data_start,
        data_finish,
        len(data)
    ))


    # find peaks - pulse
    res, num = find_data_peaks(
        data=data,
        data_freq=frequency,
        height=5,
        peak_dt=0.05,
        prominence=150,
    )
    if res != 0:
        print('Error finding peaks')
        exit()

    if len(num) == 0:
        print('Find no peaks')
        exit()

    # готовим массив интервалов
    dts = np.array([])
    last = 0
    for p in np.nditer(num):
        dts = np.append(dts, p - last)
        last = p

    sto = np.std(dts)
    avg = np.mean(dts)
    print('std=%s avg=%s' % (sto, avg))

    # если стд отклонение слишком велико - пробуем фильтрануть и пересчитать пики
    if sto > avg / 5:
        print('bad std, filter')

        # filter
        order = 4
        try:
            f_data = butter_band_filter(
                data=data,
                lowcut=0.2,
                highcut=4,
                frequency=frequency,
                order=order,
            )

        except:
            exc_str = traceback.format_exc()
            mess = "Exception: {err}".format(err=exc_str)
            print(mess)
            exit()

        data = f_data

        # find peaks - pulse
        res, num = find_data_peaks(
            data=data,
            data_freq=frequency,
            height=5,
            peak_dt=0.05,
            prominence=150,
        )
        if res != 0:
            print('Error finding peaks')
            exit()

        if len(num) == 0:
            print('Find no peaks')
            exit()

        # готовим массив интервалов
        dts = np.array([])
        last = 0
        for p in np.nditer(num):
            dts = np.append(dts, p - last)
            last = p

        sto = np.std(dts)
        avg = np.mean(dts)
        print('std=%s avg=%s' % (sto, avg))
        if sto > avg / 5:
            print('bad std, drop')
            exit()


    # для каждого интервала, если он вылезает за пределы среднего более 3 стд отклонений
    # либо удаляем, если слишком мелкий
    # либо пытаемся разделить если большой
    new_num = np.array([], dtype=int)
    next_plus = 0       # добавка от предыдущего удаленного интервала
    skipped = 0         # сколько отсчетов пришлось на удаленные пики
    for i in range(0, len(num) - 1):
        val = num[i]
        if i >= 1:
            val = num[i] - num[i-1]
        nval = num[i+1] - num[i]
        pval = 0
        if i >= 2:
            pval = num[i-1] - num[i-2]

        print('val=%s' % val)

        # учитываем возможную добавку от удаленного предыдущего интервала
        if next_plus > 0:
            val += next_plus
            next_plus = 0
            print('add from prev %s' % next_plus)

        if val - avg < - SIGMA_FACTOR * sto:
            # короткий - смотрим можно ли объединить

            if val < avg * 0.6:
                # короткий - ищем куда присоединить, к следующему или предыдущему
                # присоединяем к кратчайшему
                if pval > nval:
                    # присоединяем к следующему
                    next_plus = val
                    print('short - add to next')
                else:
                    # присоединям к предыдущему
                    new_num[-1] += val
                    print('short - add to prev')

                continue

            else:
                # недостаточно короткий
                print('short - just drop')
                skipped += val
                continue

        if val - avg > SIGMA_FACTOR * sto:
            # длинный - смотрим можно ли разбить

            if val > avg * 1.7:
                # делим
                divider = int(val / avg)
                if val / avg - divider > 0.5:
                    divider += 1

                print('long - divide by %s' % divider)

                for d in range(divider):
                    prev = 0
                    if len(new_num) > 0:
                        prev = new_num[-1]
                    new_val = prev + int(val / divider) + skipped
                    new_num = np.append(new_num, new_val)
                    skipped = 0

                    print('append divided %s' % new_val)

                continue

            else:
                # недостаточно длинные, дропаем
                print('long - just drop')
                skipped += val
                continue

        # не вышел за пределы 3 стд откл - просто добавляем интервал
        prev = 0
        if len(new_num) > 0:
            prev = new_num[-1]
        new_val = prev + val + skipped
        new_num = np.append(new_num, new_val)
        skipped = 0

        print('normal - append %s' % new_val)



    # рисуем график если заказано
    if DO_GRAPH == 1:
        print('Draw graph final')
        try:
            res = graph(
                data=data,
                data_freq=frequency,
                fname=dout + 'DATA_final__part_%s.png' % (part + 1),
                points=new_num,
                sec_per_cm=1,
            )
        except:
            print('Exception during graphic creation')


    # выбрасываем слишком большие и мелкие промежутки
    lt = 0
    ldt = 0
    minute_stat = {}
    data = []
    for p in np.nditer(new_num):
        t = p / frequency * 1000
        dt = int(t - lt)
        print('p=%s t=%s dt=%s' % (p, t, dt))

        if dt < 300:
            # слишком большие промежутки - артефакт, не учитываем
            print('low dt=%s pass' % dt)
            continue

        lt = t

        if dt > 1500:
            # слишком большие промежутки - артефакт, не учитываем
            print('big dt=%s pass' % dt)
            continue

        if ldt > 0:
            if (dt > 1000 and dt > ldt * 1.33) or \
               (dt < 400 and dt < ldt * 0.75):
                # слишком сильно отличается от предыдущего измерения, не учитываем
                print('dt=%s ldt=%s pass' % (dt, ldt))
                continue

        dat = {
            'num': p,
            't': int(t),
            'dt': dt,
        }
        data.append(dat)

        minute = int(t / (60 * 1000))
        if minute in minute_stat:
            # уже была статистика по этой минуте
            minute_stat[minute]['rr'].append(dt)

        else:
            # еще не было статистики по этой минуте
            minute_stat[minute] = {'rr': [dt,],}

        ldt = dt



    """
    Для каждой минуты вычисляем метрики:
        - СТД отклонение длительности вошедших в минуту кардиоинтервалов (STD) в секундах
        - Моду Мо - тот временной отрезок на которой пришлось самое большое число кардиоинтервалов, в секундах.
            Для этого берем временные отрезки, округленные до 50 мс, от самого короткого интервала до самого длинного,
            смотрим в какой отрезок попало больше всего интервалов.
        - Амплитуду моды (АМо) - число кардиоинтервалов, соответствующих значению моды, в % к объему выборки.
        - Индекс напряжения (Ин) по формуле  Ин = АМо / (2 * Мо * STD)
    """
    print('Count metrics')
    mod_div = 50
    for minute in minute_stat:
        st = minute_stat[minute]
        if len(st['rr']) == 0:
            continue

        st['min'] = min(st['rr'])
        st['max'] = max(st['rr'])
        st['std'] = round(np.std(st['rr']) / 1000, 3)

        # определяем отрезки для вычисления моды
        min_d = st['min'] // mod_div
        max_d = st['max'] // mod_div
        if st['max'] != mod_div * max_d:
            max_d += 1

        mods = {}
        for mod in range(min_d, max_d + 1):
            mods[mod] = 0

        # считаем сколько интервалов в какую моду попали
        for rr in st['rr']:
            mods[rr // mod_div] += 1

        # ищем максимальную
        max_m = 0
        max_i = 0
        for mod in mods:
            if max_m < mods[mod]:
                max_i = mod
                max_m = mods[mod]

        # тот временной отрезок на которой пришлось самое большое число кардиоинтервалов
        st['Mo'] = round((mod_div * (2 * max_i + 1) / 2) / 1000, 2)

        # число кардиоинтервалов, соответствующих значению моды, в % к объему выборки
        st['AMo'] = round(max_m / len(st['rr']) * 100, 2)

        # Индекс напряжения (Ин) по формуле  Ин = АМо / (2 * Мо * STD)
        st['In'] = 0
        if st['Mo'] * st['std'] != 0:
            st['In'] = round(st['AMo'] / (2 * st['Mo'] * st['std']), 2)


    # обогащаем данные посчитанными поминутными метриками
    for i in range(len(data)):
        dat = data[i]

        # находим минуту с которой брать статистику
        t = dat['t']
        minute = int(t / (60 * 1000))
        st = minute_stat[minute]

        #
        for name in ('std', 'Mo', 'AMo', 'In'):
            dat[name] = st[name]

        data[i] = dat



    # пишем csv
    fout = dout + 'pulse_part_%s.csv' % (part + 1)
    with open(fout, 'w', newline='') as csvfile:
        fieldnames = ['num', 't', 'dt', 'std', 'Mo', 'AMo', 'In',]
        writer = csv.DictWriter(
            csvfile,
            fieldnames=fieldnames,
            delimiter=OUT_DIVISOR,
        )
        writer.writeheader()
        for dat in data:
            writer.writerow(dat)


exit()



