#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
logger = logging.getLogger(__name__)

import pprint
pp = pprint.PrettyPrinter(indent=4)

import os
import sys
import json
import codecs
import traceback
import numpy as np
from edf.functions import (
    butter_band_filter,
)
from edf.edf import EDF
from copy import deepcopy

din = "samples/"
#fin = "GarantEEG.bdf"
#fin = "BAlertEEG.edf"                # не читает корректно надо доделывать!!!!
fin = "NTrendEEG21.bdf"
fname = din + fin

dout = "samples/"
fout = dout + "filtered_" + fin

low_filter = [0.5, 5, 10, 50]
low_filter_r = [0.5, 5, 10, 50]
high_filter = [5, 10, 50, 100]
high_filter_r = [5, 10, 50, 100]
order = 3

e = EDF()

res = e.open_file('debug/debug.log', 'w', 'debug')
if res != 0:
    print('Cant open debug file')
    exit()

res = e.open_file(
    fname=fname,
    right='r',
    typ='header',
)
if res != 0:
    e.debug(e.error)
    exit()
e.debug("Open header - OK!")

pp.pprint(e.last_result)

# read all data
need_canals = list(range(0, 21))
res = e.open_file(
    fname=fname,
    right='r',
    typ='data',
    offset=0,
    cnt=0,
    need_canals=need_canals,
)
if res != 0:
    print(e.error)
    exit()
print("Read data - OK!")

res = e.open_file(fout, 'w', 'out')
if res != 0:
    print(e.error)
    exit()
print("Open out - OK!")

in_edf = e.files['data']
inparams = in_edf['params']
sc = inparams['signal_count']
duration = inparams['duration']

out_edf = e.files['out']
out_edf['params']['duration'] = duration

canal_names = [
    'Fp1',
    'F7',
    'F8',
    'T4',
    'T6',
    #'T5',
    #'T3',
    #'Fp2',
    #'O1',
    #'P3',
    #'Pz',
    #'F3',
    #'Fz',
    #'F4',
    #'C4',
    #'P4',
    #'Pz',                       # вместо POz
    #'C3',
    #'Cz',
    #'O2',
    #'BDF Annotations',         # пока не понятно как стд средствами правильно писать канал аннотаций
]

# добавляем по очереди все нужные каналы в выходн файл
for canal_name in canal_names:

    canal_num = inparams['canal_names'].get(canal_name)
    if canal_num is None:
        print('Cant find number for canal name = {cn}'.format(cn=canal_name))
        exit()

    print('Canal num = %s' % canal_num)

    summary = deepcopy(inparams['canals'][canal_num])
    data = np.asarray(inparams['canal_data'][canal_num])
    sample_rate = summary['sample_rate']
    frequency = summary['frequency']
    label = summary['label']

    print('work channel=%s sample_rate=%s freq==%s' % (label, sample_rate, frequency))


    # добавляем исх канал
    res = e.out_add_channel(summary, data)
    if res != 0:
        print(e.error)
        exit()
    print("Add channel %s - OK!\n" % label)


    fl = len(low_filter)
    for i in range(fl):

        # фильтруем
        flabel = '{label}_f_{low}_{high}'.format(
            label=label,
            low=low_filter[i],
            high=high_filter[i],
        )
        print("Add filtered channel %s" % flabel)

        try:
            new_data = butter_band_filter(
                data=data,
                lowcut=low_filter[i],
                highcut=high_filter[i],
                frequency=frequency,
                order=order,
            )

        except:
            exc_str = traceback.format_exc()
            mess = "Exception: {err}".format(err=exc_str)
            print(mess)
            continue

        # перефигачиваем в выходной файл
        new_summary = deepcopy(summary)
        new_summary['label'] = flabel
        res = e.out_add_channel(new_summary, new_data)
        if res != 0:
            print(e.error)
            exit()

        print("Add channel %s - OK!\n" % flabel)

# сохраняем выходной файл
res = e.out_save()
if res != 0:
    print(e.error)
    exit()

print("Save - OK!")





exit()
